﻿namespace PriceEntryDAL
{
    public static class DataAccessLayerService
    {
        public static ISupplierRepository SupplierRepository()
        {
            ISupplierRepository obj = new supplierRepository();
            return obj;
        }
        public static ISupplierChargeRepository SupplierChargeRepository()
        {
            ISupplierChargeRepository obj = new supplierChargeRepository();
            return obj;
        }
        public static IclassifyRepository ClassifyRepository()
        {
            IclassifyRepository obj = new classifyRepository();
            return obj;
        }
        public static ImatRepository MatRepository()
        {
            ImatRepository obj = new matRepository();
            return obj;
        }
        public static IgreenRepository GreenRepository()
        {
            IgreenRepository obj = new greenRepository();
            return obj;
        }
        public static IpickingRepository PickingRepository()
        {
            IpickingRepository obj = new pickingRepository();
            return obj;
        }
        public static ImatrcRepository MatRcRepository()
        {
            ImatrcRepository obj = new matRcRepository();
            return obj;
        }
        public static IsuppliergreenpriceRepository SupplierGreenPriceRepository()
        {
            IsuppliergreenpriceRepository obj = new suppliergreenpriceRepository();
            return obj;
        }
    }
}
