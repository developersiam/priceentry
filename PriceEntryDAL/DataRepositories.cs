﻿using DomainModel;

namespace PriceEntryDAL
{
    public interface ISecurityRepository : IGenericDataRepository<security> { }
    public class securityRepository : GenericDataRepository<security>, ISecurityRepository { }
    public interface ISupplierRepository : IGenericDataRepository<supplier> { }
    public class supplierRepository : GenericDataRepository<supplier>, ISupplierRepository { }
    public interface ITypeRepository : IGenericDataRepository<type> { }
    public class typeRepository : GenericDataRepository<type>, ITypeRepository { }
    public interface ISupplierChargeRepository : IGenericDataRepository<suppliercharge> { }
    public class supplierChargeRepository : GenericDataRepository<suppliercharge>, ISupplierChargeRepository { }
    public interface ImatRepository : IGenericDataRepository<mat> { }
    public class matRepository : GenericDataRepository<mat>, ImatRepository { }
    public interface IclassifyRepository : IGenericDataRepository<classify> { }
    public class classifyRepository : GenericDataRepository<classify>, IclassifyRepository { }
    public interface IgreenRepository : IGenericDataRepository<green> { }
    public class greenRepository : GenericDataRepository<green>, IgreenRepository { }
    public interface IpickingRepository : IGenericDataRepository<picking> { }
    public class pickingRepository : GenericDataRepository<picking>, IpickingRepository { }
    public interface ImatrcRepository : IGenericDataRepository<matrc> { }
    public class matRcRepository : GenericDataRepository<matrc>, ImatrcRepository { }
    public interface IsuppliergreenpriceRepository : IGenericDataRepository<suppliergreenprice> { }
    public class suppliergreenpriceRepository : GenericDataRepository<suppliergreenprice>, IsuppliergreenpriceRepository { }

}
