﻿using System;
using System.Collections.Generic;
using System.Linq;
using DomainModel;

namespace PriceEntryDAL
{
    public interface IStoreProcedureRepository
    {
        string sp_Receiving_DecodePassword(string pw);
        List<sp_Sel_Green_All_By_Crop_Result> sp_Sel_Green_All_By_Crop(int? crop, string type);
        List<sp_GreenLeaf_SEL_SupplierCharge_Date_Result> sp_GreenLeaf_SEL_SupplierCharge_Date(string supplier, DateTime? ReceivingDate);
        List<sp_GreenLeaf_SEL_ReceivingDocInfo_Result> sp_GreenLeaf_SEL_ReceivingDocInfo(int crop, string type);
        List<sp_Receiving_SEL_MatByDocNo_Result> sp_Receiving_SEL_MatByDocNo_Info(int Docno);
        List<sp_Receiving_SEL_Company_Result> sp_Receiving_SEL_CompanyInfo();
    }
    public class StoreProcedureRepository : IStoreProcedureRepository
    {
        public string sp_Receiving_DecodePassword(string pw)
        {
            try
            {
                string result = "";
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    var password = _context.sp_Receiving_DecodePassword(pw).ToList();
                    if (password.Any())
                    {
                        result = password[0];
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Sel_Green_All_By_Crop_Result> sp_Sel_Green_All_By_Crop(int? crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Sel_Green_All_By_Crop(crop, type).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GreenLeaf_SEL_SupplierCharge_Date_Result> sp_GreenLeaf_SEL_SupplierCharge_Date(string supplier, DateTime? ReceivingDate)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GreenLeaf_SEL_SupplierCharge_Date(supplier, ReceivingDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> sp_GreenLeaf_SEL_SupplierCharge_DocNo(string supplier, DateTime? ReceivingDate, string docNo)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GreenLeaf_SEL_SupplierCharge_DocNo(supplier, ReceivingDate,docNo).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GreenLeaf_SEL_DocTruck_ByDate_Result> sp_GreenLeaf_SEL_DocTruck_ByDate(string supplier, DateTime? ReceivingDate)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GreenLeaf_SEL_DocTruck_ByDate(supplier, ReceivingDate).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_GreenLeaf_SEL_ReceivingDocInfo_Result> sp_GreenLeaf_SEL_ReceivingDocInfo(int crop, string type)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_GreenLeaf_SEL_ReceivingDocInfo(crop, type).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Receiving_SEL_MatByDocNo_Result> sp_Receiving_SEL_MatByDocNo_Info(int Docno)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Receiving_SEL_MatByDocNo(Docno).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<sp_Receiving_SEL_Company_Result> sp_Receiving_SEL_CompanyInfo()
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Receiving_SEL_Company().OrderBy(c => c.code).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
