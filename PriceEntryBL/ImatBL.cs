﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface ImatBL
    {
        //List<int?> GetDocNo(string Supplier, DateTime RecDate);
        //List<string> GetTruck(string Supplier, DateTime RecDate);
        bool CheckClassifyUsed(int crop, string type, string Cgrade);
        bool CheckGreenUsed(int crop, string type, string Ggrade);
        List<mat> GetMatInfoByCrop(int crop, string type);
        List<mat> GetMatListByRcNo(string rcNo);
        void UpdatePricePerBales(mat item);
        mat GetSingle(string bc);
        void UpdateLeafLocked(int? docNo, string rcNo);

    }
    public class matBL : ImatBL
    {
        public bool CheckClassifyUsed(int crop, string type, string Cgrade)
        {
            bool ex = false;
            var t = DataAccessLayerService.MatRepository().GetList(m => m.crop == crop &&
                                                                        m.type == type && 
                                                                        m.classify == Cgrade && m.issued == false);
          
            if (t.Any()) ex = true;

            return ex;
        }
        public bool CheckGreenUsed(int crop, string type, string Ggrade)
        {
            bool ex = false;
            var t = DataAccessLayerService.MatRepository().GetList(m => m.crop == crop &&
                                                                        m.type == type &&
                                                                        m.green == Ggrade && m.issued == false);

            if (t.Any()) ex = true;

            return ex;
        }
        public List<mat> GetMatInfoByCrop(int crop, string type)
        {
            var t = DataAccessLayerService.MatRepository().GetList(m => m.crop == crop &&
                                                                        m.type == type &&
                                                                        m.docno != null).ToList();
            if (t.Any()) return t;
            return null;
        }
        public List<mat> GetMatListByRcNo(string rcNo)
        {
            var rc = DataAccessLayerService.MatRepository().GetList(m => m.rcno == rcNo && m.green != "BACK").ToList();
            if (rc.Any()) return rc;
            return null;
        }
        public mat GetSingle(string barcode)
        {
            return DataAccessLayerService.MatRepository().GetSingle(m => m.bc == barcode);
        }
        public void UpdatePricePerBales(mat item)
        {
            var selected_bales = GetSingle(item.bc);
            if (selected_bales != null)
            {
                selected_bales.priceno = item.priceno;
                selected_bales.priceunit = item.priceunit;
                selected_bales.price = item.price;
                selected_bales.tcharge = item.tcharge;
                selected_bales.handle_charge = item.handle_charge;
                selected_bales.price_wo_handle_charge = item.price_wo_handle_charge;
                selected_bales.priceunit_wo_handle_charge = item.priceunit_wo_handle_charge;

                DataAccessLayerService.MatRepository().Update(selected_bales);
            }           
        }
        public void UpdateLeafLocked(int? docNo, string rcNo)
        {
            if (rcNo != null)
            {
                var selected_docNo = GetMatListByRcNo(rcNo).Where(l => l.docno == docNo);
                foreach (var i in selected_docNo)
                {
                    var selected_bales = GetSingle(i.bc);
                    if (selected_bales != null)
                    {
                        selected_bales.leaflocked = true;

                        DataAccessLayerService.MatRepository().Update(selected_bales);
                    }
                }
            }
            

        }
        //public List<int?> GetDocNo(string Supplier, DateTime RecDate)
        //{
        //    matRepository mat = new matRepository();
        //    var allDocNo = new List<int?>();
        //    allDocNo = mat.GetList(m => m.supplier == Supplier && m.dtrecord == RecDate)
        //               .OrderBy(r => r.docno).
        //               Select(w => w.docno).Distinct().ToList();
        //    //allDocNo = mat.GetAll().OrderBy(c => c.docno).Select(w => w.docno).Distinct().ToList();
        //    return allDocNo;
        //}
        //public List<string> GetTruck(string Supplier, DateTime RecDate)
        //{
        //    matRepository mat = new matRepository();
        //    var allDocNo = new List<int?>();
        //    allDocNo = mat.GetList(m => m.supplier == Supplier && m.dtrecord == RecDate)
        //               .OrderBy(r => r.docno).
        //               Select(w => w.docno).Distinct().ToList();
        //    //allDocNo = mat.GetAll().OrderBy(c => c.docno).Select(w => w.docno).Distinct().ToList();
        //    return allDocNo;
        //}

    }
}