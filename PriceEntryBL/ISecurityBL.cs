﻿using DomainModel;
using PriceEntryDAL;
using System.Linq;

namespace PriceEntryBL
{
    public interface ISecurityBL
    {
        bool ExistedUser(string un);
        security GetAuthen(string un);
    }

    public class SecurityBL : ISecurityBL
    {
        public bool ExistedUser(string un)
        {
            bool existedUser = false;
            securityRepository s = new securityRepository();
            var user = s.GetList(w => w.uname == un);
            if (user.Any())
                existedUser = true;

            return existedUser;
        }
        public security GetAuthen(string un)
        {
            securityRepository s = new securityRepository();
            return s.GetSingle(x => x.uname == un);
        }
    }
}