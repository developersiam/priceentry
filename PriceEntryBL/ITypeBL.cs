﻿using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface ITypeBL
    {
        List<string> GetAllTypes();
        List<type> GetAll();
    }
    public class TypeBL : ITypeBL
    {
        public List<type> GetAll()
        {
            typeRepository sp = new typeRepository();
            return sp.GetAll().ToList();
        }

        public List<string> GetAllTypes()
        {
            typeRepository sp = new typeRepository();
            var allt = new List<string>();
            allt = sp.GetAll().OrderBy(c => c.type1).Select(w => w.type1).Distinct().ToList();
            return allt;
        }
    }
}