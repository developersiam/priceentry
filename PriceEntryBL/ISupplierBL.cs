﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;


namespace PriceEntryBL
{
    public interface ISupplierBL
    {
        List<supplier> GetAllSupplier();
        List<string> GetAllSupplierCode();
        string GetSupplierName(string code);
        bool ExistedSupp(string code);
        void UpdateSupp(supplier item);
        void AddNewSupp(supplier item);
        void Deletesupp(supplier item);
    }
    public class SupplierBL : ISupplierBL
    {
        public List<supplier> GetAllSupplier()
        {
            return DataAccessLayerService.SupplierRepository().GetAll().ToList();
        }
        public List<string> GetAllSupplierCode()
        {
            supplierRepository sp = new supplierRepository();
            var allSP = new List<string>();
            allSP = sp.GetAll().OrderBy(c => c.code).Select(w => w.code).Distinct().ToList();
            return allSP;
        }
        public string GetSupplierName(string code)
        {
            supplierRepository sp = new supplierRepository();
            string spName = "";
            var supplierN = sp.GetSingle(w => w.code == code);
            if (supplierN != null)
                spName = supplierN.name;

            return spName;
        }
        public bool ExistedSupp(string code)
        {
            bool existedBC = false;
            supplierRepository p = new supplierRepository();
            var ex = p.GetList(w => w.code == code);
            if (ex.Count() == 1)
            {
                existedBC = true;
            }
            return existedBC;
        }
        public void UpdateSupp(supplier item)
        {
            DataAccessLayerService.SupplierRepository().Update(item);
        }
        public void AddNewSupp(supplier item)
        {
            DataAccessLayerService.SupplierRepository().Add(item);
        }
        public void Deletesupp(supplier item)
        {
            supplierRepository s = new supplierRepository();
            var _deleteModel = s.GetSingle(c => c.code == item.code);

            if (_deleteModel == null)
                throw new ArgumentException("ไม่พบข้อมูล");
            DataAccessLayerService.SupplierRepository().Remove(_deleteModel);
        }
    }
}
