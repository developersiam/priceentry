﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface IsuppliergreenpriceBL
    {
        bool ExistedsupplierSetup(decimal crop,string supplierCode , string type, string companyCode, int priceNo);
        void UpdateSuppliergreenprice(suppliergreenprice item);
        void AddNewSuppliergreenprice(suppliergreenprice item);
        List<suppliergreenprice> GetsupplierGreenPrice(decimal crop);
        void UpdateSupplierStatus(suppliergreenprice item, bool status);
        int GetPriceNo(int? crop, string supplierCode, string type, string compCode);
        suppliergreenprice GetSupplierSetup(decimal crop, string supplierCode, string type, string companyCode, int priceNo);
        void InactiveAllprice(suppliergreenprice item);
    }
    public class suppliergreenpriceBL : IsuppliergreenpriceBL
    {
        public void InactiveAllprice(suppliergreenprice item)
        {
            List<suppliergreenprice> lstsetup;
            lstsetup = DataAccessLayerService.SupplierGreenPriceRepository().GetList(s => s.crop == item.crop && s.type == item.type && s.code == item.code
                                           && s.companycode == item.companycode).ToList();
            foreach (var t in lstsetup)
            {
                var sp = new suppliergreenprice
                {
                    crop = t.crop,
                    code = t.code,
                    type = t.type,
                    companycode = t.companycode,
                    pricecode = t.pricecode,
                    dtrecord = t.dtrecord,
                    user = t.user,
                };
                UpdateSupplierStatus(sp, false);
            }
        }
        public suppliergreenprice GetSupplierSetup(decimal crop, string supplierCode, string type, string companyCode, int priceNo)
        {
            suppliergreenprice existed = DataAccessLayerService.SupplierGreenPriceRepository().GetSingle(s => s.crop == crop && s.type == type && s.code == supplierCode
                                           && s.companycode == companyCode && s.pricecode == priceNo);

            return existed;
        }
        public bool ExistedsupplierSetup(decimal crop, string supplierCode, string type, string companyCode, int priceNo)
        {
            bool ex = false;
            var existed = DataAccessLayerService.SupplierGreenPriceRepository().GetList(s => s.crop == crop && s.type == type && s.code == supplierCode 
                                           && s.companycode == companyCode && s.IsActive == true);
            if (existed.Any()) ex = true;

            return ex;
        }
        public void UpdateSuppliergreenprice(suppliergreenprice item)
        {
            DataAccessLayerService.SupplierGreenPriceRepository().Update(item);
        }
        public void UpdateSupplierStatus(suppliergreenprice item, bool status)
        {
            var existed = DataAccessLayerService.SupplierGreenPriceRepository().GetSingle(s => s.crop == item.crop && s.type == item.type && s.code == item.code
                                           && s.companycode == item.companycode && s.pricecode == item.pricecode);

            if (existed != null)
            {
                var old_item = new suppliergreenprice
                {
                    crop = existed.crop,
                    code = existed.code,
                    name = existed.name,
                    type = existed.type,
                    companycode = existed.companycode,
                    pricecode = existed.pricecode,
                    IsActive = status,
                    dtrecord = DateTime.Now,
                    user = existed.user
                };
                DataAccessLayerService.SupplierGreenPriceRepository().Update(old_item);
            }
         }

        public void AddNewSuppliergreenprice(suppliergreenprice item)
        {
            DataAccessLayerService.SupplierGreenPriceRepository().Add(item);
        }
        public List<suppliergreenprice> GetsupplierGreenPrice(decimal crop)
        {
            List<suppliergreenprice> rc = DataAccessLayerService.SupplierGreenPriceRepository().GetList(m => m.IsActive == true && m.crop == crop).ToList();

            return rc;
        }
        public int GetPriceNo(int? crop, string supplierCode, string type, string compCode)
        {
            int pNo = 0;

            var existed = DataAccessLayerService.SupplierGreenPriceRepository().GetSingle(s => s.crop == crop && s.type == type && s.code == supplierCode
                                           && s.companycode == compCode && s.IsActive == true);
            if (existed != null ) pNo = existed.pricecode;

            return pNo;
        }
    }
 }
