﻿using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;
using System;
using Microsoft.SqlServer.Server;

namespace PriceEntryBL
{
    public interface ISupplierChargeBL
    {
        bool ExistedCharge(string code, string t, string green);
        void AddNewCharge(suppliercharge charge);
        void Add(string code, string name, string type, string green,
            double farmerBonus, double transportCharge, double serviceFee,
            double procurementFee, double twFee, string username);
        void Edit(string code, string name, string type, string green,
            double farmerBonus, double transportCharge, double serviceFee,
            double procurementFee, double twFee, string username);
        void Delete(List<suppliercharge> list);

        void Copy(string fromSupplierCode, string toSupplierCode, string toSupplierName, List<suppliercharge> suppliercharges, string username);
        void AdjustPrice(List<suppliercharge> supplierChargeList);
        void UpdateCharge(suppliercharge item);
        List<suppliercharge> GetSupplierAllGrade(string code, string t);
        void DeleteCharge(suppliercharge item);
        bool ExistedSupplierCharge(string code, string t);
        void ReproductionSupplier(string fromCode, string toCode, string toName, string t, string userName);
        List<suppliercharge> GetSupplierByType(string t);
        suppliercharge GetSingle(string supplierCode, string type, string green);

    }
    public class SupplierChargeBL : ISupplierChargeBL
    {
        public bool ExistedCharge(string code, string t, string green)
        {
            bool existedBC = false;
            supplierChargeRepository p = new supplierChargeRepository();
            var ex = p.GetList(w => w.code == code && w.type == t && w.green == green);
            if (ex.Count() > 0)
            {
                existedBC = true;
            }
            return existedBC;
        }
        public void AddNewCharge(suppliercharge newCharge)
        {
            DataAccessLayerService.SupplierChargeRepository().Add(newCharge);
        }
        public void UpdateCharge(suppliercharge item)
        {
            DataAccessLayerService.SupplierChargeRepository().Update(item);
        }
        public List<suppliercharge> GetSupplierAllGrade(string code, string t)
        {
            supplierChargeRepository s = new supplierChargeRepository();
            var chargeList = s.GetList(c => c.code == code && c.type == t).ToList();
            return chargeList;
        }
        public void DeleteCharge(suppliercharge item)
        {
            supplierChargeRepository s = new supplierChargeRepository();
            var _deleteModel = s.GetSingle(c => c.code == item.code && c.type == item.type && c.green == item.green);

            if (_deleteModel == null)
                throw new ArgumentException("ไม่พบข้อมูล");


            DataAccessLayerService.SupplierChargeRepository().Remove(_deleteModel);
        }
        public bool ExistedSupplierCharge(string code, string t)
        {
            bool existed = false;
            supplierChargeRepository p = new supplierChargeRepository();
            var ex = p.GetList(w => w.code == code && w.type == t);
            if (ex.Count() > 0)
            {
                existed = true;
            }
            return existed;
        }
        public void ReproductionSupplier(string from, string to, string toName, string t, string userName)
        {
            //get all record from supplier
            supplierChargeRepository p = new supplierChargeRepository();
            var ex = p.GetList(w => w.code == from && w.type == t);
            {
                foreach (var item in ex)
                {
                    var toSupplierList = new suppliercharge
                    {
                        code = to,
                        name = (string)toName,
                        type = t,
                        green = item.green,
                        farmer_bonus = item.farmer_bonus,
                        tcharge = item.tcharge,
                        fee = item.fee,
                        service_fee = item.service_fee,
                        tw_fee = item.tw_fee,
                        dtrecord = DateTime.Now,
                        user = userName
                    };
                    DataAccessLayerService.SupplierChargeRepository().Add(toSupplierList);
                }
            }
        }
        public List<suppliercharge> GetSupplierByType(string t)
        {
            supplierChargeRepository s = new supplierChargeRepository();
            var chargeList = s.GetList(c => c.type == t).ToList();
            return chargeList;
        }
        public suppliercharge GetSingle(string supplierCode, string type, string green)
        {
            supplierChargeRepository s = new supplierChargeRepository();
            return s.GetSingle(x => x.code == supplierCode
            && x.type == type
            && x.green == green);
        }

        public void Add(string code, string name, string type, string green,
            double farmerBonus, double transportCharge, double serviceFee,
            double procurementFee, double twFee, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                    throw new Exception("Supplier code cannot be empty.");

                if (string.IsNullOrEmpty(name))
                    throw new Exception("Supplier name cannot be empty.");

                if (string.IsNullOrEmpty(type))
                    throw new Exception("Type cannot be empty.");

                if (string.IsNullOrEmpty(green))
                    throw new Exception("Green cannot be empty.");

                var model = GetSingle(code, type, green);
                if (model != null)
                    throw new Exception("Supplier charge ของเกรด "
                        + green
                        + " มีอยู่แล้วในระบบ");

                DataAccessLayerService.SupplierChargeRepository()
                    .Add(new suppliercharge
                    {
                        code = code,
                        name = name,
                        type = type,
                        green = green,
                        farmer_bonus = farmerBonus,
                        tcharge = transportCharge,
                        fee = procurementFee,
                        service_fee = serviceFee,
                        tw_fee = twFee,
                        user = username,
                        dtrecord = DateTime.Now
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string code, string name, string type, string green,
            double farmerBonus, double transportCharge, double serviceFee,
            double procurementFee, double twFee, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                    throw new Exception("Supplier code cannot be empty.");

                if (string.IsNullOrEmpty(name))
                    throw new Exception("Supplier name cannot be empty.");

                if (string.IsNullOrEmpty(type))
                    throw new Exception("Type cannot be empty.");

                if (string.IsNullOrEmpty(green))
                    throw new Exception("Green cannot be empty.");

                var model = GetSingle(code, type, green);
                if (model == null)
                    throw new Exception("Supplier charge of " + green + " cannot be null.");

                model.farmer_bonus = farmerBonus;
                model.tcharge = transportCharge;
                model.service_fee = serviceFee;
                model.fee = procurementFee;
                model.tw_fee = twFee;
                model.dtrecord = DateTime.Now;
                model.user = username;

                DataAccessLayerService.SupplierChargeRepository()
                    .Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(List<suppliercharge> list)
        {
            try
            {
                foreach (suppliercharge item in list)
                {
                    var model = DataAccessLayerService.SupplierChargeRepository()
                        .GetSingle(x => x.code == item.code
                        && x.type == item.type
                        && x.green == item.green);
                    if (model == null)
                        throw new Exception("ไม่พบเกรด " + item.green + " ในระบบ");
                }

                foreach (suppliercharge item in list)
                {
                    DataAccessLayerService.SupplierChargeRepository()
                        .Remove(item);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Copy(string fromSupplierCode, string toSupplierCode, string toSupplierName, List<suppliercharge> suppliercharges, string username)
        {
            try
            {
                if (suppliercharges.Count() <= 0)
                    throw new Exception("โปรดเลือกรายการข้อมูลที่ต้องการ copy อย่างน้อย 1 รายการ");

                var type = suppliercharges.FirstOrDefault().type;
                var destinationList = DataAccessLayerService.SupplierChargeRepository()
                    .GetList(x => x.code == toSupplierCode
                    && x.type == type
                    )
                    .ToList();

                var dupplicatedList = (from a in destinationList
                                       from b in suppliercharges
                                       where a.green == b.green
                                       select a)
                                      .ToList();

                if (dupplicatedList.Count() > 0)
                    throw new Exception("ชุดข้อมูลที่ต้องการ copy มีซ้ำอยู่แล้วใน "
                        + toSupplierCode
                        + " จำนวน "
                        + dupplicatedList.Count + " รายการ");

                suppliercharges.ForEach(x =>
                {
                    x.code = toSupplierCode;
                    x.name = toSupplierName;
                    x.dtrecord = DateTime.Now;
                    x.user = username;
                });

                DataAccessLayerService.SupplierChargeRepository()
                        .Add(suppliercharges.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AdjustPrice(List<suppliercharge> supplierChargeList)
        {
            try
            {
                if (supplierChargeList.Count() <= 0)
                    throw new Exception("ไม่พบรายการชุดข้อมูลที่ต้องการปรับราคา");

                DataAccessLayerService.SupplierChargeRepository()
                    .Update(supplierChargeList.ToArray());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}