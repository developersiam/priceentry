﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface IgreenBL
    {
        List<green> GetAllGreenByCropType(decimal crop, string type);
        bool ExistedGreen(decimal crop,string type, string green);
        void UpdateGreen(green item);
        void AddNewGreen(green item);
        void DeleteGreen(green item);
    }
    public class greenBL : IgreenBL
    {
        public bool ExistedGreen(decimal crop,string t, string grade)
        {
            bool ex = false;
            var existed = DataAccessLayerService.GreenRepository().GetList(s => s.crop == crop && s.type == t && s.green1 == grade).ToList();
            if (existed.Any()) ex = true;

            return ex;
        }
        public void UpdateGreen(green item)
        {
            DataAccessLayerService.GreenRepository().Update(item);
        }
        public void AddNewGreen(green item)
        {
            DataAccessLayerService.GreenRepository().Add(item);
        }
        public void DeleteGreen(green item)
        {
           DataAccessLayerService.GreenRepository().Remove(item);
        }
        public List<green> GetAllGreenByCropType(decimal crop, string t)
        {
            return DataAccessLayerService.GreenRepository().GetList(s => s.type == t && s.crop == crop && s.green1 != "BACK").OrderBy(s => s.green1).ToList();
        }

    }
}