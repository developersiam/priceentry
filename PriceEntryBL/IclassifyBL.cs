﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface IclassifyBL
    {
        List<classify> GetAllClassify();
        List<classify> GetClassifyByType(string t, bool l);

        bool ExistedClassify(string grade, bool l, string t);
        void AddNewClassify(classify NewItem);
        void EditClassify(classify Item);
        void DeleteClassify(classify Item);
    }
    public class classifyBL : IclassifyBL
    {
        public List<classify> GetAllClassify()
        {
          return  DataAccessLayerService.ClassifyRepository().GetAll().ToList();
        }
        public List<classify> GetClassifyByType(string t, bool l)
        {
            return DataAccessLayerService.ClassifyRepository().GetList(s => s.type == t && s.Laos == l).OrderBy(s => s.Quality).ThenBy(s => s.level).ToList();
        }
        public void AddNewClassify(classify newItem)
        {
            DataAccessLayerService.ClassifyRepository().Add(newItem);
        }
        public void EditClassify(classify i)
        {
            DataAccessLayerService.ClassifyRepository().Update(i);
        }
        public void DeleteClassify(classify i)
        {
            DataAccessLayerService.ClassifyRepository().Remove(i);
        }
        public bool ExistedClassify(string grade, bool l,string type)
        {
            bool ex = false;
            var t = DataAccessLayerService.ClassifyRepository().GetList(s => s.classify1 == grade && s.Laos == l && s.type == type).ToList();
            if (t.Any()) ex = true;

            return ex;

        }
    }
}