﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PriceEntryBL
{
    public static class BussinessLayerService
    {
        public static ISecurityBL SecurityBL()
        {
            ISecurityBL obj = new SecurityBL();
            return obj;
        }
        public static IStoreProcedureBL StoreProcedureBL()
        {
            IStoreProcedureBL obj = new StoreProcedureBL();
            return obj;
        }
        public static ISupplierBL SupplierBL()
        {
            ISupplierBL obj = new SupplierBL();
            return obj;
        }
        public static ITypeBL TypeBL()
        {
            ITypeBL obj = new TypeBL();
            return obj;
        }
        public static ISupplierChargeBL SupplierChargeBL()
        {
            ISupplierChargeBL obj = new SupplierChargeBL();
            return obj;
        }
        public static ImatBL matBL()
        {
            ImatBL obj = new matBL();
            return obj;
        }
        public static IclassifyBL classifyBL()
        {
            IclassifyBL obj = new classifyBL();
            return obj;
        }
        public static IgreenBL greenBL()
        {
            IgreenBL obj = new greenBL();
            return obj;
        }
        public static IpickingBL pickingBL()
        {
            IpickingBL obj = new pickingBL();
            return obj;
        }
        public static ImatRCBL matRcBL()
        {
            ImatRCBL obj = new matrcBL();
            return obj;
        }
        public static IsuppliergreenpriceBL suppliergreenpriceBL()
        {
            IsuppliergreenpriceBL obj = new suppliergreenpriceBL();
            return obj;
        }
    }
}
