﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface ImatRCBL
    {
        List<matrc> GetMatRcInfoByCrop(int crop, string type);
        matrc GetMatRcInfoByRcNo(string rcno);
        
    }
    public class matrcBL : ImatRCBL
    {
        public List<matrc> GetMatRcInfoByCrop(int crop, string type)
        {
            var t = DataAccessLayerService.MatRcRepository().GetList(m => m.crop == crop &&
                                                                        m.type == type &&
                                                                        m.rcfrom == "Purchasing" &&
                                                                        m.checkerlocked == true).ToList();
            if (t.Any()) return t;
            return null;
        }
        public matrc GetMatRcInfoByRcNo(string rcno)
        {
            return DataAccessLayerService.MatRcRepository().GetSingle(m => m.rcno == rcno);
        }
    }
}