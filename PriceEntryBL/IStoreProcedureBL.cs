﻿using PriceEntryDAL;
using System.Collections.Generic;
using DomainModel;
using System;
using System.Linq;

namespace PriceEntryBL
{
    public interface IStoreProcedureBL
    {
        string DecodePW(string pw);
        List<sp_Sel_Green_All_By_Crop_Result> GetGreenCode(int? crop, string type);
        List<sp_GreenLeaf_SEL_SupplierCharge_Date_Result> GetSupplierChargeByDate(string supplier, DateTime? ReceivingDate);
        List<string> GetDocumentwithSupplier(string supplier, DateTime? ReceivingDate);
        List<string> GetTruckwithSupplier(string supplier, DateTime? ReceivingDate);
        List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> GetSupplierChargeByDoc(string supplier, DateTime? ReceivingDate, string DocNo);
        List<sp_GreenLeaf_SEL_ReceivingDocInfo_Result> GetReceivingInfo(int crop, string type);
        List<sp_Receiving_SEL_MatByDocNo_Result> GetReceivingInfoByDoc(int Docno);
        List<sp_Receiving_SEL_Company_Result> GetCompanyList();
        string GetCompanyNameByCode(string code);

    }
    public class StoreProcedureBL : IStoreProcedureBL
    {
        public string DecodePW(string pw)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            string firstDecode = "";
            var loopNum = new List<int> { 0, 1 };

            firstDecode = sp.sp_Receiving_DecodePassword(pw);
            foreach (int i in loopNum)
            {
                firstDecode = sp.sp_Receiving_DecodePassword(firstDecode);
            }
            return firstDecode;
        }
        public List<sp_Sel_Green_All_By_Crop_Result> GetGreenCode(int? crop, string type)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Sel_Green_All_By_Crop(crop, type);
        }
        public List<sp_GreenLeaf_SEL_SupplierCharge_Date_Result> GetSupplierChargeByDate(string supp, DateTime? rec_date)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_GreenLeaf_SEL_SupplierCharge_Date(supp, rec_date);
        }
        public List<string> GetDocumentwithSupplier(string supplier, DateTime? ReceivingDate)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            var docList = new List<string>();
            var allList = sp.sp_GreenLeaf_SEL_DocTruck_ByDate(supplier, ReceivingDate).ToList();
            foreach (var i in allList)
            {
                if (docList != null && !docList.Contains(i.docno.ToString()))
                    if (i.docno != 0) docList.Add(i.docno.ToString());
                else if (docList != null)
                    if (i.docno != 0) docList.Add(i.docno.ToString());
            }
            return docList;
        }
        public List<string> GetTruckwithSupplier(string supplier, DateTime? ReceivingDate)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            var truckList = new List<string>();
            var allList = sp.sp_GreenLeaf_SEL_DocTruck_ByDate(supplier, ReceivingDate);
            foreach (var i in allList)
            {
                if (truckList != null && !truckList.Contains(i.truckno.ToString()))
                    truckList.Add(i.truckno.ToString());
                else if (truckList != null)
                    truckList.Add(i.truckno.ToString());
            }
            return truckList;
        }
        public List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> GetSupplierChargeByDoc(string supp, DateTime? rec_date, string docno)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_GreenLeaf_SEL_SupplierCharge_DocNo(supp, rec_date, docno);
        }
        public List<sp_GreenLeaf_SEL_ReceivingDocInfo_Result> GetReceivingInfo(int crop, string type)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_GreenLeaf_SEL_ReceivingDocInfo(crop, type);
        }
        public List<sp_Receiving_SEL_MatByDocNo_Result> GetReceivingInfoByDoc(int Docno)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            return sp.sp_Receiving_SEL_MatByDocNo_Info(Docno);
        }
        public List<sp_Receiving_SEL_Company_Result> GetCompanyList()
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            var allList = sp.sp_Receiving_SEL_CompanyInfo();

            return allList;
        }
        public string GetCompanyNameByCode(string c)
        {
            StoreProcedureRepository sp = new StoreProcedureRepository();
            string compName;
            compName = "";

            var allList = sp.sp_Receiving_SEL_CompanyInfo();
            foreach (var i in allList)
            {
                if (string.IsNullOrEmpty(compName)) { if (i.code == c) compName = i.name; }
                else break;
            }

            return compName;
        }
    }
}
