﻿using System;
using System.Collections.Generic;
using PriceEntryDAL;
using DomainModel;
using System.Linq;

namespace PriceEntryBL
{
    public interface IpickingBL
    {
        List<picking> GetPickingByType(string t);
        void AddNewPicking(picking item);
        void UpdatePicking(picking item);
        void DeletePicking(picking item);
        bool ExistedPicking(string t, string grade);
    }
    public class pickingBL : IpickingBL
    {
        public List<picking> GetPickingByType(string t)
        {
            return DataAccessLayerService.PickingRepository().GetList(s => s.type == t).OrderBy(s => s.classify).ToList();
        }
        public void AddNewPicking(picking item)
        {
            DataAccessLayerService.PickingRepository().Add(item);
        }
        public void UpdatePicking(picking item)
        {
            DataAccessLayerService.PickingRepository().Update(item);
        }
        public void DeletePicking(picking item)
        {
            DataAccessLayerService.PickingRepository().Remove(item);
        }
        public bool ExistedPicking(string t, string grade)
        {
            bool ex = false;
            var existed = DataAccessLayerService.PickingRepository().GetList(s => s.type == t && s.classify == grade).ToList();            
            if (existed.Any()) ex = true;

            return ex;
        }
    }
}
