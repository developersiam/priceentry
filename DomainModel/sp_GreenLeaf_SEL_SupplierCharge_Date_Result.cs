﻿using System;

namespace DomainModel
{
    public partial class sp_GreenLeaf_SEL_SupplierCharge_Date_Result
    {
        public string green { get; set; }
        public Int32 bt { get; set; }
        public decimal wt { get; set; }
        public double punit { get; set; }
        public string type { get; set; }
        public double price { get; set; }
        public double farmerbonus_c { get; set; }
        public double farmerbonus { get; set; }
        public decimal tcharge_c { get; set; }
        public decimal tcharge { get; set; }
        public double ProcurementFee_c { get; set; }
        public double ProcurementFee { get; set; }
        public double ServiceFee_c { get; set; }
        public double ServiceFee { get; set; }
        public double TWFee_c { get; set; }
        public double TWFee { get; set; }
    }
    public partial class GreenLeaf_SupplierCharge_Date
    {
        public string green { get; set; }
        public Int32 bt { get; set; }
        public decimal wt { get; set; }
        public double unitPrice { get; set; }
        public double price { get; set; }
        public double farmerbonus_c { get; set; }
        public double farmerbonus { get; set; }
        public decimal tcharge_c { get; set; }
        public decimal tcharge { get; set; }
        public double ProcurementFee_c { get; set; }
        public double ProcurementFee { get; set; }
        public double ServiceFee_c { get; set; }
        public double ServiceFee { get; set; }
        public double TWFee_c { get; set; }
        public double TWFee { get; set; }
        public double avgPrices { get; set; }
        public double totalPrices { get; set; }
    }
}