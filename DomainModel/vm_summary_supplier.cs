﻿using System;

namespace DomainModel
{
    public class vm_summary_supplier
    {
        public string rcno { get; set; }
        public int docno { get; set; }
        public string type { get; set; }
        public DateTime rcdate { get; set; }
        public string green { get; set; }
        public string supplier { get; set; }
        public string truckno { get; set; }
        public string buyer { get; set; }
        public string classifier { get; set; }
        public int bale { get; set; }
        public Nullable<decimal> weightbuy { get; set; }
        public Nullable<double> priceunit_wo_handle_charge { get; set; }
        public Nullable<double> priceunit { get; set; }
        public Nullable<double> price { get; set; }
        public Nullable<double> pricetotal { get; set; }
        public decimal weighttotal { get; set; }
        public decimal priceavg { get; set; }
    }
    public class vm_summary_stec
    {
        public string rcno { get; set; }
        public int docno { get; set; }
        public string type { get; set; }
        public DateTime rcdate { get; set; }
        public string green { get; set; }
        public string supplier { get; set; }
        public string truckno { get; set; }
        public string buyer { get; set; }
        public Nullable<decimal> tcharge { get; set; }
        public string classifier { get; set; }
        public int bale { get; set; }
        public Nullable<decimal> weightbuy { get; set; }
        public Nullable<double> priceunit_wo_handle_charge { get; set; }
        public Nullable<double> priceunit { get; set; }
        public Nullable<double> price { get; set; }
        public Nullable<double> pricetotal { get; set; }
        public decimal weighttotal { get; set; }
        public decimal priceavg { get; set; }
    }
    public class vm_details_supplier
    {
        public string rcno { get; set; }
        public string bc { get; set; }
        public int docno { get; set; }
        public string type { get; set; }
        public DateTime rcdate { get; set; }
        public string green { get; set; }
        public string supplier { get; set; }
        public string truckno { get; set; }
        public string buyer { get; set; }
        public Nullable<decimal> tcharge { get; set; }
        public Nullable<double> handleCharge { get; set; }
        public string classifier { get; set; }
        public int bale { get; set; }
        public Nullable<decimal> weightbuy { get; set; }
        public Nullable<double> priceunit_wo_handle_charge { get; set; }
        public Nullable<double> pric_wo_handle_charge { get; set; }
        public Nullable<double> priceunit { get; set; }
        public Nullable<double> price { get; set; }
    }
}
