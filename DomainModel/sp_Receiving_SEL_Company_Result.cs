﻿using System;

namespace DomainModel
{
    public partial class sp_Receiving_SEL_Company_Result
    {
        public string code { get; set; }
        public string name { get; set; }
        public string remark { get; set; }
        public Nullable<DateTime> dtrecord { get; set; }
        public string  user { get; set; }
        public bool FromBuyingSystem { get; set; }
        public bool FromLoadBales { get; set; }
    }
}
