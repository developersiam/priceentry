﻿using System;

namespace DomainModel
{
    public partial class sp_Sel_Green_All_By_Crop_Result
    {
        public decimal crop { get; set; }
        public string type { get; set; }
        public string green { get; set; }
        public decimal? price1 { get; set; }
        public decimal? price2 { get; set; }
        public decimal? price3 { get; set; }
        public decimal? price4 { get; set; }
        public decimal? price5 { get; set; }
        public decimal? price6 { get; set; }
        public decimal? price7 { get; set; }
        public decimal? price8 { get; set; }
        public decimal? price9 { get; set; }
        public decimal? price10 { get; set; }
        public decimal? price11 { get; set; }
        public decimal? price12 { get; set; }
        public decimal? price13 { get; set; }
        public decimal? price14 { get; set; }
        public decimal? price15 { get; set; }
        public Int32? group { get; set; }
        public Int32? level { get; set; }
    }
}