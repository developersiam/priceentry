﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public partial class sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result
    {
        public Int32 docno { get; set; }
        public string green { get; set; }
        public Int32 bt { get; set; }
        public decimal wt { get; set; }
        public double punit { get; set; }
        public string type { get; set; }
        public string buyer { get; set; }
        public string classifier { get; set; }
        public double price { get; set; }
        public double farmerbonus_c { get; set; }
        public double farmerbonus { get; set; }
        public decimal tcharge_c { get; set; }
        public decimal tcharge { get; set; }
        public double ProcurementFee_c { get; set; }
        public double ProcurementFee { get; set; }
        public double ServiceFee_c { get; set; }
        public double ServiceFee { get; set; }
        public double TWFee_c { get; set; }
        public double TWFee { get; set; }
        public string truckno { get; set; }

    }
    public partial class GreenLeaf_SupplierCharge_Doc
    {
        public string green { get; set; }
        public Int32 bt { get; set; }
        public decimal wt { get; set; }
        public double unitPrice { get; set; }
        public double price { get; set; }
        public double farmerbonus_c { get; set; }
        public double farmerbonus { get; set; }
        public decimal tcharge_c { get; set; }
        public decimal tcharge { get; set; }
        public double ProcurementFee_c { get; set; }
        public double ProcurementFee { get; set; }
        public double ServiceFee_c { get; set; }
        public double ServiceFee { get; set; }
        public double TWFee_c { get; set; }
        public double TWFee { get; set; }
        public string truckno { get; set; }
        public double avgPrices { get; set; }
        public double totalPrices { get; set; }
    }
}
