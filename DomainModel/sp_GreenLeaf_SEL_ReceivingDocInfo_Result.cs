﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public partial class sp_GreenLeaf_SEL_ReceivingDocInfo_Result
    {
        public DateTime? date { get; set; }
        public Nullable<int> docno { get; set; }
        public string supplier { get; set; }
        public string company { get; set; }
        public string rcno { get; set; }
        public string type { get; set; }
        public string place { get; set; }
        public string truckno { get; set; }
        public string buyer { get; set; }
        public string classifier { get; set; }
        public string rcfrom { get; set; }
        public bool? checkerlocked { get; set; }
        public Nullable<bool> leaflocked { get; set; }
        public int bt { get; set; }
        public Nullable<int> crop { get; set; }
    }
}
