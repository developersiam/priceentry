﻿namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    public partial class green
    {
        public decimal crop { get; set; }
        public string type { get; set; }
        public string green1 { get; set; }
        public int? level { get; set; }
        public Nullable<decimal> p1 { get; set; }
        public Nullable<decimal> p2 { get; set; }
        public Nullable<decimal> p3 { get; set; }
        public Nullable<decimal> p4 { get; set; }
        public Nullable<decimal> p5 { get; set; }
        public Nullable<decimal> p6 { get; set; }
        public Nullable<decimal> p7 { get; set; }
        public Nullable<decimal> p8 { get; set; }
        public Nullable<decimal> p9 { get; set; }
        public Nullable<decimal> p10 { get; set; }
        public Nullable<decimal> p11 { get; set; }
        public Nullable<decimal> p12 { get; set; }
        public Nullable<decimal> p13 { get; set; }
        public Nullable<decimal> p14 { get; set; }
        public Nullable<decimal> p15 { get; set; }
        public Nullable<DateTime> dtrecord { get; set; }
        public string rcuser { get; set; }
        public string leafuser { get; set; }
        public int? group { get; set; }

    }
}