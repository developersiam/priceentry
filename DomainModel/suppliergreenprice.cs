﻿using System;

namespace DomainModel
{
    public partial class suppliergreenprice
    {
        public int crop { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string type { get; set; }
        public string companycode { get; set; }
        public int pricecode { get; set; }
        public bool IsActive { get; set; }
        public DateTime dtrecord { get; set; }
        public string user { get; set; }

        public virtual sp_Receiving_SEL_Company_Result sp_Receiving_SEL_Company_Result { get; set; }

    }


}
