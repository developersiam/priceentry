﻿using System;
namespace DomainModel
{
    public class sp_Receiving_SEL_MatByDocNo_Result
    {
        public string green { get; set; }
        public int bt { get; set; }
        public Nullable<decimal> wt { get; set; }
        public Nullable<double> punit { get; set; }
        public Nullable<double> price { get; set; }
        public Nullable<decimal> tcharge { get; set; }
        public Nullable<double> handle_charge { get; set; }
        public Nullable<double> priceunit_wo_handle_charge { get; set; }        
        public Nullable<double> price_wo_handle_charge { get; set; }
        public Nullable<double> total_price_wo { get; set; }
        public string supplier { get; set; }
    }
}