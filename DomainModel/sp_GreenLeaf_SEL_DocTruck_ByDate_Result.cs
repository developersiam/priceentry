﻿using System;

namespace DomainModel
{
    public partial class sp_GreenLeaf_SEL_DocTruck_ByDate_Result
    {
        public Int32 docno { get; set; }
        public string truckno { get; set; }
    }
}
