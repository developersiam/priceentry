﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace PriceEntry.Model
{
    public class vm_supplierCharge : suppliercharge
    {
        public string rec_no { get; set; }
        public string truck_no { get; set; }
        public DateTime rec_date { get; set; }
    }
}
