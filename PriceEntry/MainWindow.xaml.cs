﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PriceEntry.Forms;

namespace PriceEntry
{
    public partial class MainWindow : Window
    {
        string user = "";
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(string UserName)
        {
            InitializeComponent();
            user = UserName;
        }

        public void SupplierPriceMenu_Click(object sender, RoutedEventArgs e)
        {
            //SupplierPrice charge = new SupplierPrice(user);
            //charge.Title = string.Format("Price Entry System - [{0}]", user);
            //MainFrame.Navigate(charge);
            var page = new MVVM.View.SupplierPrice();
            var vm = new MVVM.ViewModel.vmSupplierPrice();
            page.DataContext = vm;
            MainFrame.Navigate(page);
        }
        private void OnCopy(object sender, ExecutedRoutedEventArgs e)
        {

        }
        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        public void LogoffMenu_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("ต้องการออกจากโปรแกรมหรือไม่", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) Close();
        }

        public void SupplierManageMenu_Click(object sender, RoutedEventArgs e)
        {
            Supplier s = new Supplier();
            s.Title = string.Format("Price Entry System - [{0}]", user);
            MainFrame.Navigate(s);
        }

        private void ReceivingChargeReport_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new Reports.RPTSupplierRec_ByDate(user));
        }

        private void ReceivingChargeDetailsReport_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new Reports.RPTSupplierRec_ByDoc(user));
        }

        private void GreenMenu_Click(object sender, RoutedEventArgs e)
        {
            GreenPage g = new GreenPage(user);
            MainFrame.Navigate(g);
        }

        private void ClassifyMenu_Click(object sender, RoutedEventArgs e)
        {
            ClassifyPage c = new ClassifyPage(user);
            MainFrame.Navigate(c);
        }

        private void PriceSetupMenu_Click(object sender, RoutedEventArgs e)
        {
            PriceSetupPage p = new PriceSetupPage(user);
            MainFrame.Navigate(p);
        }

        private void PickingMenu_Click(object sender, RoutedEventArgs e)
        {
            PickingPage picking = new PickingPage(user);
            MainFrame.Navigate(picking);
        }

        private void PriceEntry_Click(object sender, RoutedEventArgs e)
        {
            ReceivingPage rec = new ReceivingPage(user);
            MainFrame.Navigate(rec);
        }

        private void SupplierPriceSetupMenu_Click(object sender, RoutedEventArgs e)
        {
            PriceSupplierSetupPage suppPrice = new PriceSupplierSetupPage(user);
            MainFrame.Navigate(suppPrice);
        }
    }
}
