﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Reporting.WinForms;
using DomainModel;
using PriceEntryBL;

namespace PriceEntry.Reports
{
    /// <summary>
    /// Interaction logic for RPTSummarySupplier.xaml
    /// </summary>
    public partial class RPTDetailsStec : Window
    {
        int Curr_docNo;
        string Curr_rcNo;
        public RPTDetailsStec(string Rcno,int Docno)
        {
            InitializeComponent();
            Curr_docNo = Docno;
            Curr_rcNo = Rcno;
            ReloadReport();
        }

        private void ReloadReport()
        {
            try
            {
                //Get Truck and Buyer info
                matrc driver_details = BussinessLayerService.matRcBL().GetMatRcInfoByRcNo(Curr_rcNo);

                //List<sp_Receiving_SEL_MatByDocNo_Result> rc_detail = new List<sp_Receiving_SEL_MatByDocNo_Result>();
                //rc_detail.AddRange(BussinessLayerService.StoreProcedureBL().GetReceivingInfoByDoc(Curr_docNo));

                //Get all barcode for calculate
                List<mat> rec_detail = new List<mat>();
                rec_detail.AddRange(BussinessLayerService.matBL().GetMatListByRcNo(Curr_rcNo).Where(rc => rc.docno == Curr_docNo).OrderBy(w => w.green));

                if (rec_detail != null)
                {
                    List<vm_details_supplier> t = new List<vm_details_supplier>(rec_detail.Select(r => new vm_details_supplier
                    {
                        rcno = driver_details.rcno,
                        docno = Curr_docNo,
                        type = driver_details.type,
                        supplier = r.supplier,
                        green = r.green,
                        bale = (int)r.baleno,
                        rcdate = (DateTime)driver_details.date,
                        weightbuy = (r.company == "10" ? r.weight : r.weightbuy),
                        buyer = driver_details.buyer,
                        truckno = driver_details.truckno,
                        classifier = driver_details.classifier,
                        priceunit_wo_handle_charge = r.priceunit_wo_handle_charge,
                        priceunit = r.priceunit,
                        price = r.price,
                        handleCharge = r.handle_charge,
                        tcharge = r.tcharge,
                        pric_wo_handle_charge = r.price_wo_handle_charge,
                        bc = r.bc
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_details_supplier";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PriceEntry.Reports.RDLC.Rpt004.rdlc";
                    ReportViewer.RefreshReport();
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
