﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Reporting.WinForms;
using DomainModel;
using PriceEntryBL;

namespace PriceEntry.Reports
{
    /// <summary>
    /// Interaction logic for RPTSummarySupplier.xaml
    /// </summary>
    public partial class RPTSummaryStec : Window
    {
        int Curr_docNo;
        string Curr_rcNo;
        decimal total_weight = 0;
        double total_price = 0;
        double total_price_supplier = 0;
        public RPTSummaryStec(string Rcno,int Docno)
        {
            InitializeComponent();
            Curr_docNo = Docno;
            Curr_rcNo = Rcno;
            ReloadReport();
        }

        private void ReloadReport()
        {
            try
            {
                total_weight = 0;
                total_price = 0;
                total_price_supplier = 0;

                //Get Truck and Buyer info
                matrc driver_details = BussinessLayerService.matRcBL().GetMatRcInfoByRcNo(Curr_rcNo);

                List<sp_Receiving_SEL_MatByDocNo_Result> rc_detail = new List<sp_Receiving_SEL_MatByDocNo_Result>();
                rc_detail.AddRange(BussinessLayerService.StoreProcedureBL().GetReceivingInfoByDoc(Curr_docNo));

                if (rc_detail != null)
                {
                    foreach (var i in rc_detail)
                    {
                        total_weight += (decimal)i.wt;

                        //price total
                        total_price += (double)i.price;
                        total_price_supplier += (double)i.price_wo_handle_charge;
                    }

                    List<vm_summary_stec> t = new List<vm_summary_stec>(rc_detail.Select(r => new vm_summary_stec
                    {
                        rcno = driver_details.rcno,
                        docno = Curr_docNo,
                        type = driver_details.type,
                        supplier = r.supplier,
                        green = r.green,
                        bale = r.bt,
                        rcdate = (DateTime)driver_details.date,
                        weightbuy = r.wt,
                        buyer = driver_details.buyer,
                        truckno = driver_details.truckno,
                        classifier = driver_details.classifier,
                        priceunit_wo_handle_charge = r.priceunit_wo_handle_charge,
                        priceunit = r.punit,
                        price = r.price,
                        pricetotal = r.total_price_wo,
                        weighttotal = total_weight,
                        priceavg = Convert.ToDecimal(total_price) / total_weight,
                        tcharge = r.tcharge
                    }));

                    ReportDataSource datasource = new ReportDataSource();
                    datasource.Value = t;
                    datasource.Name = "vm_summary_stec_dataset";

                    ReportViewer.Reset();
                    ReportViewer.LocalReport.DataSources.Add(datasource);
                    ReportViewer.LocalReport.ReportEmbeddedResource = "PriceEntry.Reports.RDLC.Rpt002.rdlc";
                    ReportViewer.RefreshReport();
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
