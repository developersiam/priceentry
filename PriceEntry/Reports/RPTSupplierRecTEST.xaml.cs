﻿using System;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using Microsoft.Reporting.WinForms;

namespace PriceEntry.Reports
{
    /// <summary>
    /// Interaction logic for RPTSupplierRec_Date.xaml
    /// </summary>
    public partial class RPTSupplierRecTEST : Page
    {
        public RPTSupplierRecTEST()
        {
            InitializeComponent();
            BlindingData();           
        }

        private void BlindingData()
        {
            receivingDateDatePicker.SelectedDate = DateTime.Now;
            suppComboBox.ItemsSource = null;
            suppComboBox.ItemsSource = BussinessLayerService.SupplierBL().GetAllSupplierCode();
        }

        private void receivingDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReportSetup();
        }

        private void ReportSetup()
        {
            try
            {
                if (receivingDateDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("โปรดเลือกวันที่รับใบยา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ReportDataSource ds = new ReportDataSource();
               
            }
            catch
            {

            }
        }
    }
}
