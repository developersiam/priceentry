﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.IO;
using OfficeOpenXml;
using System.Text.RegularExpressions;

namespace PriceEntry.Reports
{
    /// <summary>
    /// Interaction logic for RPTSupplierRec_ByDoc.xaml
    /// </summary>
    public partial class RPTSupplierRec_ByDoc : Page
    {
        Int32 sumBt = 0;
        decimal sumWt = 0;
        double sumPrice = 0;
        double sumfarmerbonus = 0;
        decimal sumtcharge = 0;
        double sumProcurementFee = 0;
        double sumServiceFee = 0;
        double sumTWFee = 0;
        double sumTotalPrices = 0;
        string currentUser = "";

        public RPTSupplierRec_ByDoc(string user)
        {
            InitializeComponent();
            BlindingData();
            currentUser = user;
        }

        private void BlindingData()
        {
            RecDatePicker.SelectedDate = DateTime.Now;
            supplierComboBox.ItemsSource = null;
            supplierComboBox.ItemsSource = BussinessLayerService.SupplierBL().GetAllSupplierCode();
            supplierComboBox.Focus();
        }

        private void RecDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //Blinding Document
            docNoComboBox.ItemsSource = null;
            docNoComboBox.ItemsSource = BussinessLayerService.StoreProcedureBL().GetDocumentwithSupplier(supplierComboBox.Text, RecDatePicker.SelectedDate);
        }

        private void exportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (receivingDataGrid == null) return;

                //Prepare Data to List
                List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> tmpRec = new List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result>();
                tmpRec = BussinessLayerService.StoreProcedureBL().GetSupplierChargeByDoc(supplierComboBox.Text, RecDatePicker.SelectedDate, docNoComboBox.Text);
                if (tmpRec.Count() != 0)
                {
                    System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                    folderDlg.ShowNewFolderButton = true;
                    System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        string folderPath = ExportDetail(folderDlg.SelectedPath, tmpRec);
                        Environment.SpecialFolder root = folderDlg.RootFolder;
                        MessageBox.Show("Export ข้อมูลสำเร็จ.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);
                        System.Diagnostics.Process.Start(folderPath);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private string ExportDetail(string folderName, List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> tmpRec)
        {
            string tmpcolumn = "";
            string suppName = Regex.Replace(supplierComboBox.Text, @"[^0-9a-zA-Z\._]", "", RegexOptions.Compiled);
            string name = @"" + folderName + "\\SummarySheet-" + suppName + "-" + docNoComboBox.Text + "-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";

            FileInfo info = new FileInfo(name);
            if (info.Exists) File.Delete(name);

            //Calculate for Export file
            var tmpRep = new List<GreenLeaf_SupplierCharge_Doc>();
            tmpRep = CalculateSummary(tmpRec);
            tmpcolumn = (4 + tmpRep.Count).ToString();

            var truckNo = tmpRep.FirstOrDefault().truckno;

            //AVG
            var avgPunit = sumPrice == 0 ? 0 : decimal.Divide((decimal)sumPrice, sumWt);
            var avgFarmerbonus = sumfarmerbonus == 0 ? 0 : decimal.Divide((decimal)sumfarmerbonus, sumWt);
            var avgtcharge = sumtcharge == 0 ? 0 : decimal.Divide((decimal)sumtcharge, sumWt);
            var avgProcurementFee = sumProcurementFee == 0 ? 0 : decimal.Divide((decimal)sumProcurementFee, sumWt);
            var avgServiceFee = sumServiceFee == 0 ? 0 : decimal.Divide((decimal)sumServiceFee, sumWt);
            var avgTW = sumTWFee == 0 ? 0 : decimal.Divide((decimal)sumTWFee, sumWt);
            var avgPrice = sumTotalPrices / (double)sumWt;

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Summary_Sheet");
                ws.Cells["A1"].Value = "Supplier : " + supplierComboBox.Text;
                ws.Cells["A2"].Value = "Receiving Date : " + RecDatePicker.SelectedDate;
                ws.Cells["E1"].Value = "Doc no. : " + docNoComboBox.Text;
                ws.Cells["E2"].Value = "Truck no. : " + tmpRep.FirstOrDefault().truckno;
                ws.Cells["M1"].Value = "User : " + currentUser;
                ws.Cells["M2"].Value = "Print Date : " + DateTime.Now.ToShortDateString();

                ws.Cells["A3"].LoadFromCollection(tmpRep, true);
                ws.Protection.IsProtected = false;

                ws.DeleteColumn(16);

                ws.Cells["A" + tmpcolumn].Value = "Total : ";
                ws.Cells["B" + tmpcolumn].Value = sumBt;
                ws.Cells["C" + tmpcolumn].Value = sumWt;
                ws.Cells["D" + tmpcolumn].Value = Math.Round(avgPunit, 2, MidpointRounding.AwayFromZero);
                ws.Cells["E" + tmpcolumn].Value = sumPrice;
                ws.Cells["F" + tmpcolumn].Value = Math.Round(avgFarmerbonus, 2, MidpointRounding.AwayFromZero);
                ws.Cells["G" + tmpcolumn].Value = sumfarmerbonus;
                ws.Cells["H" + tmpcolumn].Value = Math.Round(avgtcharge, 2, MidpointRounding.AwayFromZero);
                ws.Cells["I" + tmpcolumn].Value = sumtcharge;
                ws.Cells["J" + tmpcolumn].Value = Math.Round(avgProcurementFee, 2, MidpointRounding.AwayFromZero);
                ws.Cells["K" + tmpcolumn].Value = sumProcurementFee;
                ws.Cells["L" + tmpcolumn].Value = Math.Round(avgServiceFee, 2, MidpointRounding.AwayFromZero);
                ws.Cells["M" + tmpcolumn].Value = sumServiceFee;
                ws.Cells["N" + tmpcolumn].Value = Math.Round(avgTW, 2, MidpointRounding.AwayFromZero);
                ws.Cells["O" + tmpcolumn].Value = sumTWFee;
                ws.Cells["P" + tmpcolumn].Value = avgPrice;
                ws.Cells["Q" + tmpcolumn].Value = sumTotalPrices;
                ws.Column(1).Width = 13;
                ws.Column(2).Width = 10;
                ws.Column(3).Width = 13;
                ws.Column(4).Width = 10;
                ws.Column(5).Width = 15;
                ws.Column(7).Width = 15;
                ws.Column(9).Width = 15;
                ws.Column(11).Width = 15;
                ws.Column(13).Width = 15;
                ws.Column(15).Width = 15;
                ws.Column(17).Width = 15;

                //Change Header Name
                ws.Cells["A3"].Value = "Buy Grade";
                ws.Cells["B3"].Value = "Bale";
                ws.Cells["C3"].Value = "Kilos";
                ws.Cells["D3"].Value = "Green Price";
                ws.Cells["E3"].Value = "";
                ws.Cells["F3"].Value = "Farmer Bonus";
                ws.Cells["G3"].Value = "";
                ws.Cells["H3"].Value = "Freight In";
                ws.Cells["I3"].Value = "";
                ws.Cells["J3"].Value = "Procurement Fee";
                ws.Cells["K3"].Value = "";
                ws.Cells["L3"].Value = "Service Fee";
                ws.Cells["M3"].Value = "";
                ws.Cells["N3"].Value = "TW Handling Charge";
                ws.Cells["O3"].Value = "";
                ws.Cells["P3"].Value = "Total Prices";
                ws.Cells["Q3"].Value = "";

                //tmpcolumn = (5 + tmpRep.Count).ToString();
                //ws.Cells["N" + tmpcolumn].Value = Convert.ToDouble(sumPrice) + Convert.ToDouble(sumfarmerbonus) + Convert.ToDouble(sumtcharge) + sumProcurementFee + sumServiceFee + sumTWFee;
                //ws.Cells["M" + tmpcolumn].Value = "Total :";

                //fix format to currency
                // (fromRow, fromColumn, toColumn, intoColumn)
                using (ExcelRange col = ws.Cells[4, 3, 5 + tmpRep.Count, 3]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 4, 5 + tmpRep.Count, 4]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 5, 5 + tmpRep.Count, 5]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 6, 5 + tmpRep.Count, 6]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 7, 5 + tmpRep.Count, 7]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 8, 5 + tmpRep.Count, 8]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 9, 5 + tmpRep.Count, 9]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 10, 5 + tmpRep.Count, 10]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 11, 5 + tmpRep.Count, 11]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 12, 5 + tmpRep.Count, 12]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 13, 5 + tmpRep.Count, 13]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 14, 5 + tmpRep.Count, 14]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 15, 5 + tmpRep.Count, 15]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 16, 5 + tmpRep.Count, 16]) { col.Style.Numberformat.Format = "#,##0.00"; }
                using (ExcelRange col = ws.Cells[4, 17, 5 + tmpRep.Count, 17]) { col.Style.Numberformat.Format = "#,##0.00"; }


                //merge some column
                using (ExcelRange col = ws.Cells[3, 4, 3, 5]) { col.Value = "Green Price"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 6, 3, 7]) { col.Value = "Farmer Bonus"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 8, 3, 9]) { col.Value = "Freight In"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 10, 3, 11]) { col.Value = "Procurement Fee"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 12, 3, 13]) { col.Value = "Service Fee"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 14, 3, 15]) { col.Value = "TW Handling Charge"; col.Merge = true; }
                using (ExcelRange col = ws.Cells[3, 16, 3, 17]) { col.Value = "Total Prices"; col.Merge = true; }

                //bold header & total row
                using (ExcelRange col = ws.Cells[3, 1, 3, 17]) { col.Style.Font.Bold = true; }
                using (ExcelRange col = ws.Cells[3, 1, 3, 17]) { col.Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center; }
                using (ExcelRange col = ws.Cells[4 + tmpRep.Count, 1, 4 + tmpRep.Count, 17]) { col.Style.Font.Bold = true; }
                //using (ExcelRange col = ws.Cells[5 + tmpRep.Count, 14, 5 + tmpRep.Count, 15]) { col.Style.Font.Bold = true; }

                //Create table
                using (ExcelRange col = ws.Cells[3, 1, 4 + tmpRep.Count, 17])
                {
                    col.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                using (ExcelRange col = ws.Cells[4 + tmpRep.Count, 1, 4 + tmpRep.Count, 17])
                {
                    col.Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    col.Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                pck.Save();
            }
            return @"" + folderName;
        }

        private void docNoComboBox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                if (supplierComboBox.Text != "" && RecDatePicker.Text != "")
                {
                    receivingDataGrid.ItemsSource = null;
                    receivingDataGrid.ItemsSource = BussinessLayerService.StoreProcedureBL().GetSupplierChargeByDoc(supplierComboBox.Text, RecDatePicker.SelectedDate, docNoComboBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private List<GreenLeaf_SupplierCharge_Doc> CalculateSummary(List<sp_GreenLeaf_SEL_SupplierCharge_DocNo_Result> reportSumList)
        {
            sumBt = 0;
            sumWt = 0;
            sumPrice = 0;
            sumfarmerbonus = 0;
            sumtcharge = 0;
            sumProcurementFee = 0;
            sumServiceFee = 0;
            sumTWFee = 0;
            sumTotalPrices = 0;

            var tmpRep = new List<GreenLeaf_SupplierCharge_Doc>();
            if (reportSumList.Count() == 0) tmpRep = null;


            foreach (var item in reportSumList)
            {
                GreenLeaf_SupplierCharge_Doc vmModel = new GreenLeaf_SupplierCharge_Doc();

                vmModel.green = item.green;
                vmModel.bt = item.bt;
                vmModel.wt = item.wt;
                vmModel.unitPrice = item.punit;
                vmModel.price = item.price;
                vmModel.farmerbonus_c = item.farmerbonus_c;
                vmModel.farmerbonus = item.farmerbonus;
                vmModel.tcharge_c = item.tcharge_c;
                vmModel.tcharge = item.tcharge;
                vmModel.ProcurementFee_c = item.ProcurementFee_c;
                vmModel.ProcurementFee = item.ProcurementFee;
                vmModel.ServiceFee_c = item.ServiceFee_c;
                vmModel.ServiceFee = item.ServiceFee;
                vmModel.TWFee_c = item.TWFee_c;
                vmModel.TWFee = item.TWFee;
                vmModel.truckno = item.truckno;
                vmModel.totalPrices = (double)item.price +
                    (double)item.farmerbonus +
                    (double)item.tcharge +
                    (double)item.ProcurementFee +
                    (double)item.ServiceFee +
                    (double)item.TWFee;
                vmModel.avgPrices = (double)vmModel.totalPrices / (double)vmModel.wt;

                //Add summary
                sumBt += item.bt;
                sumWt += item.wt;
                sumPrice += item.price;
                sumfarmerbonus += item.farmerbonus;
                sumtcharge += item.tcharge;
                sumProcurementFee += item.ProcurementFee;
                sumServiceFee += item.ServiceFee;
                sumTWFee += item.TWFee;
                sumTotalPrices += vmModel.totalPrices;

                tmpRep.Add(vmModel);
            }
            return tmpRep;
        }

        private void RecDatePicker_GotFocus(object sender, RoutedEventArgs e)
        {
            BindDocNoCombobox();
        }

        private void supplierComboBox_DropDownClosed(object sender, EventArgs e)
        {
            BindDocNoCombobox();
        }

        private void BindDocNoCombobox()
        {
            if (supplierComboBox.Text == "" || RecDatePicker.SelectedDate == null) return;
            docNoComboBox.ItemsSource = null;
            docNoComboBox.ItemsSource = BussinessLayerService.StoreProcedureBL().GetDocumentwithSupplier(supplierComboBox.Text, Convert.ToDateTime(RecDatePicker.Text));
        }
    }
}
