﻿using PriceEntry.Helper;
using PriceEntry.MVVM.Common;
using PriceEntry.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PriceEntry.MVVM.View
{
    /// <summary>
    /// Interaction logic for SupplierPrice.xaml
    /// </summary>
    public partial class SupplierPrice : Page
    {
        public SupplierPrice()
        {
            InitializeComponent();
            DataContext = new ViewModel.vmSupplierPrice();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            try
            {
                var vm = (vmSupplierPrice)DataContext;
                switch (e.PropertyName)
                {
                    case nameof(vm.code):
                        supplierComboBox.Focus();
                        break;
                    case nameof(vm.type):
                        TypeComboBox.Focus();
                        break;
                    case nameof(vm.green):
                        GreenComboBox.Focus();
                        break;
                    case nameof(vm.farmerbonus):
                        FarmerBonusTextBox.SelectAll();
                        FarmerBonusTextBox.Focus();
                        break;
                    case nameof(vm.transportcharge):
                        TransportChargeTextBox.SelectAll();
                        TransportChargeTextBox.Focus();
                        break;
                    case nameof(vm.procurementfee):
                        ProcurementFeeTextBox.SelectAll();
                        ProcurementFeeTextBox.Focus();
                        break;
                    case nameof(vm.servicefee):
                        ServiceFeeTextBox.SelectAll();
                        ServiceFeeTextBox.Focus();
                        break;
                    case nameof(vm.twfee):
                        TwFeeTextBox.SelectAll();
                        TwFeeTextBox.Focus();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
