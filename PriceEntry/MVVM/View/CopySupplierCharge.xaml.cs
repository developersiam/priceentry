﻿using PriceEntry.Helper;
using PriceEntry.MVVM.Common;
using PriceEntry.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PriceEntry.MVVM.View
{
    /// <summary>
    /// Interaction logic for CopySupplierCharge.xaml
    /// </summary>
    public partial class CopySupplierCharge : Window
    {
        public CopySupplierCharge()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }
        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            try
            {
                var vm = (vmCopySupplierCharge)DataContext;
                switch (e.PropertyName)
                {
                    case nameof(vm.FromSupplier):
                        FromSupplierComboBox.Focus();
                        break;
                    case nameof(vm.ToSupplier):
                        ToSupplierComboBox.Focus();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
