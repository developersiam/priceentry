﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriceEntry.MVVM.Model
{
    public enum SupplierChargeType
    {
        FarmerBonus,
        TransportCharge,
        ProcurementFee,
        ServiceFee,
        TWFee
    }
}
