﻿using PriceEntry.MVVM.Model;
using PriceEntryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriceEntry.MVVM.Helper
{
    public static class SupplierChargeHelper
    {
        public static List<SupplierCharge> GetBySupplierAndType(string supplier, string type)
        {
            try
            {
                var list = BussinessLayerService.SupplierChargeBL()
                    .GetSupplierAllGrade(supplier, type)
                    .Select(x => new SupplierCharge
                    {
                        code = x.code,
                        name = x.name,
                        type = x.type,
                        green = x.green,
                        farmer_bonus = x.farmer_bonus,
                        tcharge = x.tcharge,
                        fee = x.fee,
                        service_fee = x.service_fee,
                        tw_fee = x.tw_fee,
                        dtrecord = x.dtrecord,
                        user = x.user,
                        IsSelected = false
                    })
                    .ToList(); ;
                return list;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
