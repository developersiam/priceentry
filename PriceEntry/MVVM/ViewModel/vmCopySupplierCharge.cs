﻿using DomainModel;
using PriceEntry.Helper;
using PriceEntry.MVVM.Common;
using PriceEntryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PriceEntry.MVVM.ViewModel
{
    public class vmCopySupplierCharge : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmCopySupplierCharge()
        {
        }

        #region Fields
        private string _fromSupplier;

        public string FromSupplier
        {
            get { return _fromSupplier; }
            set { _fromSupplier = value; }
        }

        private string _toSupplier;

        public string ToSupplier
        {
            get { return _toSupplier; }
            set { _toSupplier = value; }
        }

        private int _totalGreen;

        public int TotalGreen
        {
            get { return _totalGreen; }
            set { _totalGreen = value; }
        }

        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private vmSupplierPrice _supplierChargeVM;

        public vmSupplierPrice SupplierChargeVM
        {
            get { return _supplierChargeVM; }
            set { _supplierChargeVM = value; }
        }
        #endregion


        #region List
        private List<supplier> _fromSupplierList;

        public List<supplier> FromSupplierList
        {
            get { return _fromSupplierList; }
            set { _fromSupplierList = value; }
        }

        private List<supplier> _toSupplierList;

        public List<supplier> ToSupplierList
        {
            get { return _toSupplierList; }
            set { _toSupplierList = value; }
        }

        private List<suppliercharge> _targetSupplierChargeList;

        public List<suppliercharge> TargetSupplierChargeList
        {
            get { return _targetSupplierChargeList; }
            set { _targetSupplierChargeList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (_totalGreen <= 0)
                    throw new Exception("โปรดระบุ supplier charge ที่ต้องการ copy อย่างน้อย 1 รายการ");

                if (string.IsNullOrEmpty(_fromSupplier))
                    throw new Exception("โปรดระบุ From Supplier");

                if (string.IsNullOrEmpty(_toSupplier))
                    throw new Exception("โปรดระบุ To Supplier");

                if (MessageBoxHelper.Question("ท่านต้อง copy ข้อมูล supplier charge นี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;
                if (string.IsNullOrEmpty(_fromSupplier))
                    throw new Exception("ไม่พบ supplier code ของชุดข้อมูลต้นฉบับ");

                if (string.IsNullOrEmpty(_toSupplier))
                    throw new Exception("ไม่พบ supplier code ปลายทางที่จะทำการ copy");

                var toSupplier = _toSupplierList.Single(x => x.code == _toSupplier);
                BussinessLayerService.SupplierChargeBL()
                    .Copy(_fromSupplier,
                    _toSupplier,
                    toSupplier.name,
                    _targetSupplierChargeList,
                    AppSetting.username
                    );
                _window.Close();
                MessageBoxHelper.Info("Copy ข้อมูลจาก "
                    + _fromSupplier
                    + " ไปยัง "
                    + _toSupplier
                    + " จำนวนทั้งสิ้น "
                    + _totalGreen
                    + " รายการสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCancelCommand;

        public ICommand OnCancelCommand
        {
            get { return _onCancelCommand ?? (_onCancelCommand = new RelayCommand(OnCancel)); }
            set { _onCancelCommand = value; }
        }

        private void OnCancel(object obj)
        {
            try
            {
                //if (MessageBoxHelper.Question("ท่านต้องออกจากหน้าจอการ copy " +
                //    "ข้อมูล supplier charge นี้ใช่หรือไม่?")
                //    == MessageBoxResult.No)
                //    return;

                _window.Close();
                return;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        public void SupplierBinding()
        {
            _fromSupplierList = BussinessLayerService.SupplierBL()
                .GetAllSupplier()
                .OrderBy(x => x.code)
                .ToList();
            RaisePropertyChangedEvent(nameof(FromSupplierList));
        }
        #endregion
    }
}
