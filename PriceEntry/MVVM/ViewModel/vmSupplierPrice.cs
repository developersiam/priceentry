﻿using DomainModel;
using PriceEntry.Helper;
using PriceEntry.MVVM.Common;
using PriceEntry.MVVM.Helper;
using PriceEntry.MVVM.Model;
using PriceEntry.MVVM.View;
using PriceEntryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace PriceEntry.MVVM.ViewModel
{
    public class vmSupplierPrice : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmSupplierPrice()
        {
            try
            {
                _supplierList = BussinessLayerService.SupplierBL()
                    .GetAllSupplier();
                _typeList = BussinessLayerService.TypeBL()
                    .GetAll();
                RaisePropertyChangedEvent(nameof(SupplierList));
                RaisePropertyChangedEvent(nameof(TypeList));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Fields
        private string _code;

        public string code
        {
            get { return _code; }
            set
            {
                _code = value;
                _name = _supplierList.SingleOrDefault(x => x.code == _code).name;
                RaisePropertyChangedEvent(nameof(name));
                SupplierChargeListBinding();
            }
        }

        private string _name;

        public string name
        {
            get { return _name; }
            set { _name = value; }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set
            {
                _type = value;
                SupplierChargeListBinding();
                _greenList = BussinessLayerService.greenBL()
                    .GetAllGreenByCropType(DateTime.Now.Year, _type);
                RaisePropertyChangedEvent(nameof(GreenList));
            }
        }

        private string _green;

        public string green
        {
            get { return _green; }
            set
            {
                _green = value;
                try
                {
                    _suppliercharge = BussinessLayerService.SupplierChargeBL()
                        .GetSingle(_code, _type, _green);
                    if (_suppliercharge == null)
                    {
                        _farmerbonus = 0;
                        _procurementfee = 0;
                        _servicefee = 0;
                        _twfee = 0;
                        _transportcharge = 0;
                    }
                    else
                    {
                        _farmerbonus = (double)_suppliercharge.farmer_bonus;
                        _procurementfee = (double)_suppliercharge.fee;
                        _servicefee = (double)_suppliercharge.service_fee;
                        _twfee = (double)_suppliercharge.tw_fee;
                        _transportcharge = (double)_suppliercharge.tcharge;
                    }
                    RaisePropertyChangedEvent(nameof(suppliercharge));
                    RaisePropertyChangedEvent(nameof(farmerbonus));
                    RaisePropertyChangedEvent(nameof(procurementfee));
                    RaisePropertyChangedEvent(nameof(servicefee));
                    RaisePropertyChangedEvent(nameof(twfee));
                    RaisePropertyChangedEvent(nameof(transportcharge));
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private double _farmerbonus;

        public double farmerbonus
        {
            get { return _farmerbonus; }
            set { _farmerbonus = value; }
        }

        private double _transportcharge;

        public double transportcharge
        {
            get { return _transportcharge; }
            set { _transportcharge = value; }
        }

        private double _procurementfee;

        public double procurementfee
        {
            get { return _procurementfee; }
            set { _procurementfee = value; }
        }

        private double _servicefee;

        public double servicefee
        {
            get { return _servicefee; }
            set { _servicefee = value; }
        }

        private double _twfee;

        public double twfee
        {
            get { return _twfee; }
            set { _twfee = value; }
        }

        private suppliercharge _suppliercharge;

        public suppliercharge suppliercharge
        {
            get { return _suppliercharge; }
            set { _suppliercharge = value; }
        }

        private string _copyFromSupplier;

        public string CopyFromSupplier
        {
            get { return _copyFromSupplier; }
            set { _copyFromSupplier = value; }
        }

        private string _copyToSupplier;

        public string CopyToSupplier
        {
            get { return _copyToSupplier; }
            set { _copyToSupplier = value; }
        }

        private bool _isSelectedAll;

        public bool IsSelectedAll
        {
            get { return _isSelectedAll; }
            set
            {
                _isSelectedAll = value;

                _supplierChargeList.ForEach(x => x.IsSelected = _isSelectedAll);
                var temp = _supplierChargeList;
                _supplierChargeList = null;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                _supplierChargeList = temp;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion


        #region List
        private List<supplier> _supplierList;

        public List<supplier> SupplierList
        {
            get { return _supplierList; }
            set { _supplierList = value; }
        }

        private List<type> _typeList;

        public List<type> TypeList
        {
            get { return _typeList; }
            set { _typeList = value; }
        }

        private List<green> _greenList;

        public List<green> GreenList
        {
            get { return _greenList; }
            set { _greenList = value; }
        }

        private List<SupplierCharge> _supplierChargeList;

        public List<SupplierCharge> SupplierChargeList
        {
            get { return _supplierChargeList; }
            set { _supplierChargeList = value; }
        }
        #endregion


        #region Command

        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_code))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier code");
                    OnFocusRequested(nameof(code));
                    return;
                }

                if (string.IsNullOrEmpty(_type))
                {
                    MessageBoxHelper.Warning("โปรดระบุ type");
                    OnFocusRequested(nameof(type));
                    return;
                }

                if (string.IsNullOrEmpty(_green))
                {
                    MessageBoxHelper.Warning("โปรดระบุ green");
                    OnFocusRequested(nameof(green));
                    return;
                }

                if (_farmerbonus <= 0)
                {
                    if (MessageBoxHelper.Question("farmer bonus ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_transportcharge <= 0)
                {
                    if (MessageBoxHelper.Question("transport charge ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_procurementfee <= 0)
                {
                    if (MessageBoxHelper.Question("procurement fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_servicefee <= 0)
                {
                    if (MessageBoxHelper.Question("service fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_twfee <= 0)
                {
                    if (MessageBoxHelper.Question("tw fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการเพิ่ม supplier charge ของเกรด "
                    + _green
                    + " ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BussinessLayerService.SupplierChargeBL()
                    .Add(_code,
                    _name,
                    _type,
                    _green,
                    _farmerbonus,
                    _transportcharge,
                    _servicefee,
                    _procurementfee,
                    _twfee,
                    AppSetting.username);
                Clear();
                SupplierChargeListBinding();
                MessageBoxHelper.Info("เพิ่มข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_code))
                    throw new Exception("โปรดเลือก supplier code");

                if (string.IsNullOrEmpty(_type))
                    throw new Exception("โปรดเลือก type");

                var selectedList = _supplierChargeList.Where(x => x.IsSelected == true);
                var records = selectedList.Count();
                if (records <= 0)
                    throw new Exception("โปรดเลือกรายการที่ต้องการลบอย่างน้อย 1 รายการ");

                if (MessageBoxHelper.Question("ท่านต้องการลบ supplier charge เกรดที่เลือกนี้จำนวน "
                    + records
                    + " รายการ ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BussinessLayerService.SupplierChargeBL()
                    .Delete(selectedList
                    .Where(x => x.IsSelected == true)
                    .Select(x => new suppliercharge
                    {
                        code = x.code,
                        name = x.name,
                        type = x.type,
                        green = x.green,
                        farmer_bonus = x.farmer_bonus,
                        tcharge = x.tcharge,
                        fee = x.fee,
                        service_fee = x.service_fee,
                        tw_fee = x.tw_fee,
                        dtrecord = x.dtrecord,
                        user = x.user
                    })
                    .ToList());

                MessageBoxHelper.Info("ลบข้อมูลจำนวน " + records + " รายการสำเร็จ");
                Clear();
                SupplierChargeListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCopyCommand;

        public ICommand OnCopyCommand
        {
            get { return _onCopyCommand ?? (_onCopyCommand = new RelayCommand(OnCopy)); }
            set { _onCopyCommand = value; }
        }

        private void OnCopy(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_code))
                {
                    MessageBoxHelper.Warning("โปรดเลือก supplier code ต้นฉบับ");
                    OnFocusRequested(nameof(code));
                    return;
                }

                if (_supplierChargeList
                    .Where(x => x.IsSelected == true)
                    .Count() <= 0)
                    throw new Exception("ไม่พบรายการข้อมูล supplier charge ต้นฉบับที่จะใช้ในการ copy" +
                        " โปรดเลือกแถวข้อมูลอย่างน้อย 1 รายการ");

                var targetSupplierChargeList = _supplierChargeList
                    .Where(x => x.IsSelected == true)
                    .Select(x => new suppliercharge
                    {
                        code = x.code,
                        name = x.name,
                        type = x.type,
                        green = x.green,
                        farmer_bonus = x.farmer_bonus,
                        tcharge = x.tcharge,
                        fee = x.fee,
                        service_fee = x.service_fee,
                        tw_fee = x.tw_fee,
                        dtrecord = DateTime.Now,
                        user = AppSetting.username
                    })
                    .ToList();

                var window = new CopySupplierCharge();
                var vm = new vmCopySupplierCharge();
                vm.FromSupplier = _code;
                vm.FromSupplierList = _supplierList;
                vm.ToSupplierList = _supplierList
                    .Where(x => x.code != _code)
                    .ToList();
                vm.TargetSupplierChargeList = targetSupplierChargeList;
                vm.TotalGreen = targetSupplierChargeList.Count;
                vm.SupplierChargeVM = this;
                vm.Window = window;
                window.DataContext = vm;
                window.ShowDialog();

                Clear();
                SupplierChargeListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAdjustPriceCommand;

        public ICommand OnAdjustPriceCommand
        {
            get { return _onAdjustPriceCommand ?? (_onAdjustPriceCommand = new RelayCommand(OnAdjustPrice)); }
            set { _onAdjustPriceCommand = value; }
        }

        private void OnAdjustPrice(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_code))
                {
                    MessageBoxHelper.Warning("โปรดเลือก supplier code ที่ต้องการ Adjust Price");
                    OnFocusRequested(nameof(code));
                    return;
                }

                if (_supplierChargeList
                    .Where(x => x.IsSelected == true)
                    .Count() <= 0)
                    throw new Exception("โปรดเลือกแถวข้อมูลที่จะทำการ Adjust Price อย่างน้อย 1 รายการ");

                var selectedList = _supplierChargeList
                    .Where(x => x.IsSelected == true)
                    .Select(x => new suppliercharge
                    {
                        code = x.code,
                        name = x.name,
                        type = x.type,
                        green = x.green,
                        farmer_bonus = x.farmer_bonus,
                        tcharge = x.tcharge,
                        fee = x.fee,
                        service_fee = x.service_fee,
                        tw_fee = x.tw_fee,
                        dtrecord = DateTime.Now,
                        user = AppSetting.username
                    })
                    .ToList();

                if (selectedList.Count() <= 0)
                    throw new Exception("โปรดเลือกรายการชุดข้อมูลที่จะทำการ copy อย่างน้อย 1 รายการ");

                var window = new AdjustPriceParameter();
                var vm = new vmAdjustPriceParameter();
                vm.PopupWindow = window;
                vm.SupplierChargeList = selectedList;
                window.DataContext = vm;
                window.ShowDialog();

                Clear();
                SupplierChargeListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            Clear();
        }

        private ICommand _onCheckedAllCommand;

        public ICommand OnCheckedAllCommand
        {
            get { return _onCheckedAllCommand ?? (_onCheckedAllCommand = new RelayCommand(OnCheckedAll)); }
            set { _onCheckedAllCommand = value; }
        }

        private void OnCheckedAll(object obj)
        {
            try
            {
                _supplierChargeList.ForEach(x => x.IsSelected = true);
                var temp = _supplierChargeList;
                _supplierChargeList = null;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                _supplierChargeList = temp;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnCheckedAllCommand;

        public ICommand OnUnCheckedAllCommand
        {
            get { return _onUnCheckedAllCommand ?? (_onUnCheckedAllCommand = new RelayCommand(OnUnCheckedAll)); }
            set { _onUnCheckedAllCommand = value; }
        }

        private void OnUnCheckedAll(object obj)
        {
            try
            {
                _supplierChargeList.ForEach(x => x.IsSelected = false);
                var temp = _supplierChargeList;
                _supplierChargeList = null;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                _supplierChargeList = temp;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDataGridDoubleClickCommand;

        public ICommand OnDataGridDoubleClickCommand
        {
            get { return _onDataGridDoubleClickCommand ?? (_onDataGridDoubleClickCommand = new RelayCommand(OnDataGridDoubleClick)); }
            set { _onDataGridDoubleClickCommand = value; }
        }

        private void OnDataGridDoubleClick(object obj)
        {
            try
            {
                var model = (SupplierCharge)obj;
                if (model == null)
                    return;

                _green = model.green;
                _farmerbonus = (double)model.farmer_bonus;
                _transportcharge = (double)model.tcharge;
                _procurementfee = (double)model.fee;
                _servicefee = (double)model.service_fee;
                _twfee = (double)model.tw_fee;
                RaisePropertyChangedEvent(nameof(green));
                RaisePropertyChangedEvent(nameof(farmerbonus));
                RaisePropertyChangedEvent(nameof(transportcharge));
                RaisePropertyChangedEvent(nameof(procurementfee));
                RaisePropertyChangedEvent(nameof(servicefee));
                RaisePropertyChangedEvent(nameof(twfee));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_code))
                {
                    MessageBoxHelper.Warning("โปรดระบุ supplier code");
                    OnFocusRequested(nameof(code));
                    return;
                }

                if (string.IsNullOrEmpty(_type))
                {
                    MessageBoxHelper.Warning("โปรดระบุ type");
                    OnFocusRequested(nameof(type));
                    return;
                }

                if (string.IsNullOrEmpty(_green))
                {
                    MessageBoxHelper.Warning("โปรดระบุ green");
                    OnFocusRequested(nameof(green));
                    return;
                }

                if (_farmerbonus <= 0)
                {
                    if (MessageBoxHelper.Question("farmer bonus ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_transportcharge <= 0)
                {
                    if (MessageBoxHelper.Question("transport charge ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_procurementfee <= 0)
                {
                    if (MessageBoxHelper.Question("procurement fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_servicefee <= 0)
                {
                    if (MessageBoxHelper.Question("service fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                if (_twfee <= 0)
                {
                    if (MessageBoxHelper.Question("tw fee ที่ท่านต้องการเพิ่มนี้ มีค่าน้อยกว่าหรือเป็นศูนย์ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;
                }

                BussinessLayerService.SupplierChargeBL()
                    .Edit(_code,
                    _name,
                    _type,
                    _green,
                    _farmerbonus,
                    _transportcharge,
                    _servicefee,
                    _procurementfee,
                    _twfee,
                    AppSetting.username);
                Clear();
                SupplierChargeListBinding();
                MessageBoxHelper.Info("แก้ไขข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        public void SupplierChargeListBinding()
        {
            try
            {
                _supplierChargeList = SupplierChargeHelper
                    .GetBySupplierAndType(_code, _type);
                _totalRecord = _supplierChargeList.Count();
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Clear()
        {
            try
            {
                _farmerbonus = 0;
                _procurementfee = 0;
                _servicefee = 0;
                _twfee = 0;
                _transportcharge = 0;
                _green = null;
                _isSelectedAll = false;

                RaisePropertyChangedEvent(nameof(farmerbonus));
                RaisePropertyChangedEvent(nameof(procurementfee));
                RaisePropertyChangedEvent(nameof(servicefee));
                RaisePropertyChangedEvent(nameof(twfee));
                RaisePropertyChangedEvent(nameof(transportcharge));
                RaisePropertyChangedEvent(nameof(green));
                RaisePropertyChangedEvent(nameof(IsSelectedAll));

                _supplierChargeList.ForEach(x => x.IsSelected = false);
                var temp = _supplierChargeList;
                _supplierChargeList = null;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                _supplierChargeList = temp;
                RaisePropertyChangedEvent(nameof(SupplierChargeList));
                SupplierChargeListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
