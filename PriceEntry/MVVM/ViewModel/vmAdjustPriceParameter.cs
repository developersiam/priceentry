﻿using DomainModel;
using PriceEntry.Helper;
using PriceEntry.MVVM.Common;
using PriceEntry.MVVM.Model;
using PriceEntryBL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace PriceEntry.MVVM.ViewModel
{
    public class vmAdjustPriceParameter : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vmAdjustPriceParameter()
        {
            try
            {
                if (_chargeTypeList == null)
                    _chargeTypeList = new List<SupplierChargeType>();

                _chargeTypeList.Add(SupplierChargeType.FarmerBonus);
                _chargeTypeList.Add(SupplierChargeType.TransportCharge);
                _chargeTypeList.Add(SupplierChargeType.ProcurementFee);
                _chargeTypeList.Add(SupplierChargeType.ServiceFee);
                _chargeTypeList.Add(SupplierChargeType.TWFee);

                RaisePropertyChangedEvent(nameof(ChargeType));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region Fields
        private double _adjustValue;

        public double AdjustValue
        {
            get { return _adjustValue; }
            set { _adjustValue = value; }
        }

        private Window _popupWindow;

        public Window PopupWindow
        {
            get { return _popupWindow; }
            set { _popupWindow = value; }
        }

        private SupplierChargeType _chargeType;

        public SupplierChargeType ChargeType
        {
            get { return _chargeType; }
            set { _chargeType = value; }
        }

        #endregion


        #region List
        private List<SupplierChargeType> _chargeTypeList;

        public List<SupplierChargeType> ChargeTypeList
        {
            get { return _chargeTypeList; }
            set { _chargeTypeList = value; }
        }

        private List<suppliercharge> _supplierChargeList;

        public List<suppliercharge> SupplierChargeList
        {
            get { return _supplierChargeList; }
            set { _supplierChargeList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_chargeType.ToString()))
                    throw new Exception("โปรดเลทอก Charge Type");

                var totalRecord = _supplierChargeList.Count();
                if (MessageBoxHelper.Question("ท่านต้องการ Adjust Price ตามที่เลือก จำนวน "
                    + totalRecord + " รายการ ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                switch (_chargeType)
                {
                    case SupplierChargeType.FarmerBonus:
                        _supplierChargeList.ForEach(x => x.farmer_bonus = _adjustValue);
                        break;
                    case SupplierChargeType.TransportCharge:
                        _supplierChargeList.ForEach(x => x.tcharge = _adjustValue);
                        break;
                    case SupplierChargeType.ProcurementFee:
                        _supplierChargeList.ForEach(x => x.fee = _adjustValue);
                        break;
                    case SupplierChargeType.ServiceFee:
                        _supplierChargeList.ForEach(x => x.service_fee = _adjustValue);
                        break;
                    case SupplierChargeType.TWFee:
                        _supplierChargeList.ForEach(x => x.tw_fee = _adjustValue);
                        break;
                    default:
                        break;
                }

                BussinessLayerService.SupplierChargeBL()
                    .AdjustPrice(_supplierChargeList);
                MessageBoxHelper.Info("Adjust Price จำนวน "
                    + totalRecord
                    + " รายการสำเร็จ");
                _popupWindow.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCloseCommand;

        public ICommand OnCloseCommand
        {
            get { return _onCloseCommand ?? (_onCloseCommand = new RelayCommand(OnClose)); }
            set { _onCloseCommand = value; }
        }

        private void OnClose(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการออกจากหน้าจอการ  Adjust Price ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                _popupWindow.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
