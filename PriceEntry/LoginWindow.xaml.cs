﻿using System;
using System.Windows;
using System.Windows.Input;
using PriceEntryBL;
using DomainModel;
using PriceEntry.Helper;

namespace PriceEntry
{
    public struct UserPacking
    {
        public string userPacking { get; set; }
    }
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            Login();

        }

        private void Login()
        {
            try
            {
                string pwd = "";
                if (!string.IsNullOrEmpty(UsernameTextbox.Text) && !string.IsNullOrEmpty(PasswordTextbox.Password))
                {
                    //Find User

                    if (BussinessLayerService.SecurityBL().ExistedUser(UsernameTextbox.Text.ToUpper()) == false)
                    {
                        MessageBox.Show("Username นี้ยังไม่มีในระบบ, กรุณาตรวจสอบข้อมูล", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        Clear();
                        return;
                    };

                    //Get Decode pw
                    security UserRole = BussinessLayerService.SecurityBL().GetAuthen(UsernameTextbox.Text.ToUpper());
                    pwd = UserRole.pwd;

                    //Decode pw
                    if (BussinessLayerService.StoreProcedureBL().DecodePW(pwd) != PasswordTextbox.Password)
                    {
                        MessageBox.Show("รหัสผ่านไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    };

                    Properties.Settings.Default.UserPriceEntry = UsernameTextbox.Text.ToUpper();
                    Properties.Settings.Default.Save();
                    AppSetting.username = UsernameTextbox.Text;

                    MainWindow main = new MainWindow(UserRole.uname);
                    main.Title = string.Format("Price Entry System - [{0}]", UserRole.uname);
                    //main.ShowDialog();
                    main.Show();
                    Close();
                }
                else
                {
                    MessageBox.Show("กรุณาระบุ Username และ Password", "Warning!!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void Clear()
        {
            UsernameTextbox.Text = "";
            PasswordTextbox.Password = "";
            UsernameTextbox.Focus();
        }

        private void PasswordTextbox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) Login();
        }
    }
}
