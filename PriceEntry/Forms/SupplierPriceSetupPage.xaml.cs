﻿using DomainModel;
using PriceEntryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using PriceEntry.Model;
namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for PriceSupplierSetupPage.xaml
    /// </summary>
    public partial class PriceSupplierSetupPage : Page
    {
        string currentUser = "";
        List<vm_suppliergreenprice> vmList;
        suppliergreenprice NewSP;
        public PriceSupplierSetupPage(string user)
        {
            InitializeComponent();
            currentUser = user;
            Blinding();
            cropTextBox.Text = Convert.ToString(DateTime.Now.Year);
            ShowSupplierPriceByCrop(cropTextBox.Text);

        }
        private void Blinding()
        {
            try
            {
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();

                //supplier
                SupplierComboBox.ItemsSource = null;
                SupplierComboBox.ItemsSource = BussinessLayerService.SupplierBL().GetAllSupplierCode();

                //company
                CompanyComboBox.ItemsSource = null;
                CompanyComboBox.ItemsSource = BussinessLayerService.StoreProcedureBL().GetCompanyList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                //var compCode = CompanyComboBox.Text.Split('|')[0];
                var compCode = (sp_Receiving_SEL_Company_Result)CompanyComboBox.SelectedItem;

                NewSP = new suppliergreenprice
                {
                    crop = Convert.ToInt16(cropTextBox.Text),
                    type = TypeComboBox.Text,
                    code = SupplierComboBox.Text,
                    companycode = compCode.code,
                    name = BussinessLayerService.SupplierBL().GetSupplierName(SupplierComboBox.Text),
                    pricecode = Convert.ToInt16(PriceComboBox.Text),
                    IsActive = true,
                    dtrecord = DateTime.Now,
                    user = currentUser
                };

                //Check the supplier has setup before
                bool existed = BussinessLayerService.suppliergreenpriceBL().ExistedsupplierSetup(Convert.ToInt16(cropTextBox.Text), SupplierComboBox.Text, TypeComboBox.Text, compCode.code, Convert.ToInt16(PriceComboBox.Text));
                if (existed)
                {
                    if (MessageBox.Show("มีการ Setup Price ให้กับ Supplier นี้ไปแล้ว, ต้องการ Update ข้อมูลใหม่ ใช่หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        //update all setup with crop,supplier,type,company,other price to false
                        BussinessLayerService.suppliergreenpriceBL().InactiveAllprice(NewSP);
                    }
                    else return;             
                };

                CheckAndUpdateSetup(NewSP);
                Blinding();
                ShowSupplierPriceByCrop(cropTextBox.Text);

                MessageBox.Show("Update ข้อมูลเรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void CheckAndUpdateSetup(suppliergreenprice NewSP)
        {
           try
            {
                //Check duplicate with crop, supplier,type, company, price
                var st = BussinessLayerService.suppliergreenpriceBL().GetSupplierSetup(Convert.ToInt16(cropTextBox.Text), SupplierComboBox.Text, TypeComboBox.Text, NewSP.companycode, Convert.ToInt16(PriceComboBox.Text));
                if (st != null)
                {
                    //Active status only user fill
                    BussinessLayerService.suppliergreenpriceBL().UpdateSupplierStatus(NewSP, true);
                }
                else
                {
                    //Add New
                    BussinessLayerService.suppliergreenpriceBL().AddNewSuppliergreenprice(NewSP);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool verifyAllInfo()
        {
            //check null
            bool cf = true;
            if (string.IsNullOrEmpty(cropTextBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Crop", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                cropTextBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                TypeComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(SupplierComboBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Supplier", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                SupplierComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(CompanyComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Company", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                CompanyComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(PriceComboBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Price", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                PriceComboBox.Focus();
                cf = false;
                return cf;
            }
            return cf;
        }
        private void PriceSetupDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PriceSetupDataGrid.SelectedIndex < 0)
                    return;

                var selected = (vm_suppliergreenprice)PriceSetupDataGrid.SelectedItem;
                if (selected != null)
                {
                    cropTextBox.Text = selected.crop.ToString();
                    SupplierComboBox.SelectedValue = selected.code;
                    TypeComboBox.SelectedValue = selected.type;
                    PriceComboBox.Text = selected.pricecode.ToString();
                    CompanyComboBox.Text = selected.companyname;
                  
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cropTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (!string.IsNullOrEmpty(cropTextBox.Text)) ShowSupplierPriceByCrop(cropTextBox.Text);
            }
        }

        private void ShowSupplierPriceByCrop(string c)
        {
            try
            {
                PriceSetupDataGrid.ItemsSource = null;
                List<suppliergreenprice> existed = BussinessLayerService.suppliergreenpriceBL().GetsupplierGreenPrice(Convert.ToDecimal(c));

                vmList = new List<vm_suppliergreenprice>();
                
                foreach (var i in existed)
                {
                       var vm = new vm_suppliergreenprice
                        {
                            crop = i.crop,
                            code = i.code,
                            type = i.type,
                            companycode = i.companycode,
                            companyname = BussinessLayerService.StoreProcedureBL().GetCompanyNameByCode(i.companycode),
                            pricecode = i.pricecode
                        };
                        vmList.Add(vm);
                  }

                PriceSetupDataGrid.ItemsSource = vmList;
            }                                               
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PriceSetupDataGrid.SelectedIndex < 0)
                    return;

                var msg = MessageBox.Show("ต้องการลบข้อมูลนี้ใช่หรือไม่?", "confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (msg == MessageBoxResult.No)
                    return;

                var model = (vm_suppliergreenprice)PriceSetupDataGrid.SelectedItem;
                if (model == null)
                    return;

                var deletesetup = new suppliergreenprice
                {
                    crop = model.crop,
                    code = model.code,
                    type = model.type,
                    companycode = model.companycode,
                    name = model.name,
                    pricecode = model.pricecode,
                    IsActive = model.IsActive,
                    user = model.user
                };
                BussinessLayerService.suppliergreenpriceBL().UpdateSupplierStatus(deletesetup, false);

                Blinding();
                ShowSupplierPriceByCrop(cropTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
