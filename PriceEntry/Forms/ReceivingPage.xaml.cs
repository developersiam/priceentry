﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Windows.Input;
using System.Windows.Media;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for ReceivingPage.xaml
    /// </summary>
    public partial class ReceivingPage : Page
    {
        string CurrentUser = "";
        public ReceivingPage(string user)
        {
            InitializeComponent();
            CurrentUser = user;
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                //type
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TypeComboBox.Text) && string.IsNullOrEmpty(cropTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ Crop และ Type ให้ครบถ้วน", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (!string.IsNullOrEmpty(cropTextBox.Text)) ShowReceivingInfo(TypeComboBox.Text, Convert.ToInt16(cropTextBox.Text));
        }

        private void ShowReceivingInfo(string t, int c)
        {
            totalRCNoTextBlock.Text = "0";
            receivingDataGrid.ItemsSource = null;
            var rec_list = BussinessLayerService.StoreProcedureBL().GetReceivingInfo(c, t);
            if (rec_list.Count() > 0)
            {
                receivingDataGrid.ItemsSource = rec_list.ToList().OrderBy(x => x.date).ThenBy(x => x.docno);
                totalRCNoTextBlock.Text = rec_list.Count().ToString();
            }
        }

        private void receivingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //double click for go to Price Entry window
            try
            {
                if (receivingDataGrid.SelectedIndex < 0)
                    return;

                ClearContent();
                var selected = (sp_GreenLeaf_SEL_ReceivingDocInfo_Result)receivingDataGrid.SelectedItem;
                if (selected != null)
                {
                    PriceEntryWindow pr = new PriceEntryWindow(CurrentUser, selected);
                    pr.ShowDialog();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearContent()
        {
            //throw new NotImplementedException();
        }

        private void receivingDataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var row = e.Row;
            var rectmp = row.DataContext as sp_GreenLeaf_SEL_ReceivingDocInfo_Result;
            if (sender == receivingDataGrid)
            {
                if (!(bool)rectmp.leaflocked)
                {
                    row.Background = new SolidColorBrush(Colors.LightPink);
                }
            }
        }
    }
}
