﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.IO;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for ClassifyPage.xaml
    /// </summary>
    public partial class ClassifyPage : Page
    {
        string currentUser = "";
        public ClassifyPage(string user)
        {
            InitializeComponent();
            currentUser = user;
            Blinding();
        }

        private void Blinding()
        {
           try
            {
                //type
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ShowClassifyGradeByType();
        }

        private void ShowClassifyGradeByType()
        {
            try
            {
                bool chkL = false;
                classifyDataGrid.ItemsSource = null;
                if ((bool)(LoasCheckBox.IsChecked)) chkL = true;
                //Show by type
                if (string.IsNullOrEmpty(TypeComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก Type", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var existed = BussinessLayerService.classifyBL().GetClassifyByType(TypeComboBox.SelectedValue.ToString(), chkL);
                if (existed.Count() > 0) classifyDataGrid.ItemsSource = existed;
                                   
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void LoasCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            ShowClassifyGradeByType();
        }

        private void LoasCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            ShowClassifyGradeByType();
        }
        private void classifyDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (classifyDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();

                var selected = (classify)classifyDataGrid.SelectedItem;
                if (selected != null)
                {
                    TypeComboBox.SelectedValue = selected.type;
                    classifyTextBox.SelectedText = selected.classify1;
                    if (selected.Quality != null) QTextBox.SelectedText = selected.Quality;
                    levelTextBox.SelectedText = selected.level.ToString();
                    if (selected.cpa_group != null) cpaTextBox.SelectedText = selected.cpa_group.ToString();
                    if ((bool)selected.Laos) LoasCheckBox.IsChecked = true;
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            //TypeComboBox.Text = "";
            classifyTextBox.Text = "";
            QTextBox.Text = "";
            levelTextBox.Text = "";
            cpaTextBox.Text = "";
            //LoasCheckBox.IsChecked = false;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
           try
            {
                
                if (verifyAllInfo())
                {
                    //check duplicate
                    if (BussinessLayerService.classifyBL().ExistedClassify(classifyTextBox.Text, (bool)LoasCheckBox.IsChecked, TypeComboBox.Text)) throw new Exception("มีเกรด Classify นี้อยู่แล้ว");
                    var i = new classify
                    {
                        classify1 = classifyTextBox.Text,
                        master = classifyTextBox.Text,
                        type = TypeComboBox.Text,
                        Quality = QTextBox.Text,
                        level = levelTextBox.Text != "" ? Convert.ToInt32(levelTextBox.Text) : (int?)null,
                        Laos = LoasCheckBox.IsChecked == true ? true : false,
                        cpa_group = cpaTextBox.Text,
                        user = currentUser,
                        dtrecord = DateTime.Now
                    };
                    BussinessLayerService.classifyBL().AddNewClassify(i);
                    MessageBox.Show("บันทึกเกรด Classify ใหม่เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ShowClassifyGradeByType();
                }
        
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                //Check classify used in mat table already
                if (BussinessLayerService.matBL().CheckClassifyUsed(DateTime.Now.Year, TypeComboBox.Text, classifyTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถลบเกรด Classify นี้ได้ เนื่องจากระบบ Receiving มีการใช้เกรดนี้ไปแล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }


                if (MessageBox.Show("ต้องการลบ Classify Grade นี้ใช่หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var i = new classify
                    {
                        classify1 = classifyTextBox.Text,
                        type = TypeComboBox.Text,
                        Laos = LoasCheckBox.IsChecked == true ? true : false,
                    };
                    BussinessLayerService.classifyBL().DeleteClassify(i);
                    MessageBox.Show("ลบข้อมูลเกรด Classify เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ShowClassifyGradeByType();
                }
                
            }
        }
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                //Check classify used in mat table already
                if (BussinessLayerService.matBL().CheckClassifyUsed(DateTime.Now.Year,TypeComboBox.Text,classifyTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถแก้ไขเกรด classify นี้ได้ เนื่องจากระบบ Receiving มีการใช้เกรดนี้ไปแล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                var i = new classify
                {
                    classify1 = classifyTextBox.Text,
                    master = classifyTextBox.Text,
                    type = TypeComboBox.Text,
                    Quality = QTextBox.Text,
                    level = levelTextBox.Text != "" ? Convert.ToInt32(levelTextBox.Text) : (int?)null,
                    Laos = LoasCheckBox.IsChecked == true ? true : false,
                    cpa_group = cpaTextBox.Text,
                    user = currentUser,
                    dtrecord = DateTime.Now
                };
                BussinessLayerService.classifyBL().EditClassify(i);
                MessageBox.Show("แก้ไขข้อมูลเกรด Classify เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowClassifyGradeByType();
            }
        }
        private bool verifyAllInfo()
        {
            //check null
            bool cf = true;
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                TypeComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(classifyTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ชื่อเกรด", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                classifyTextBox.Focus();
                cf = false;
                return cf;
            }
            //if (string.IsNullOrEmpty(QTextBox.Text))
            //{
            //    MessageBox.Show("กรุณาใส่ Quality ของเกรด", "Warning!",
            //    MessageBoxButton.OK, MessageBoxImage.Warning);
            //    QTextBox.Focus();
            //    cf = false;
            //    return cf;
            //}

            if (string.IsNullOrEmpty(levelTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ลำดับของเกรด", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                levelTextBox.Focus();
                cf = false;
                return cf;
            }
               
            return cf;
        }


    }
}
