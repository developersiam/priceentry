﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PriceEntryBL;
using DomainModel;


namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for Supplier.xaml
    /// </summary>
    public partial class Supplier : Page
    {
        public Supplier()
        {
            InitializeComponent();
            BlindingData();
        }

        private void BlindingData()
        {
            try
            {
                ShowAllSupplier();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowAllSupplier()
        {
            var existed = BussinessLayerService.SupplierBL().GetAllSupplier();
            if (existed.Count() > 0)
            {
                supplierDataGrid.ItemsSource = existed;
                PageName.Text = "Supplier [ " + Convert.ToString(existed.Count()) + " ]";             
            }
            else
                supplierDataGrid.ItemsSource = null;
        }

        private void supplierDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string name_th = "";
                string area_selected = "";
                if (supplierDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();              

                var selected = (supplier)supplierDataGrid.SelectedItem;
                if (selected != null)
                {
                    if (!string.IsNullOrEmpty(selected.nameth))
                        name_th = selected.nameth.ToString();
                    if (!string.IsNullOrEmpty(selected.area))
                        area_selected = selected.area.ToString();

                    supplierCodeTextBox.SelectedText = selected.code;
                    supplierTextBox.SelectedText = selected.name;
                    THsupplierTextBox.SelectedText = name_th;
                    areaTextBox.SelectedText = area_selected;
                    hightextBox.SelectedText = string.Format("{0:N2}", selected.qh);
                    mediumTextBox.SelectedText = string.Format("{0:N2}", selected.qm);
                    lowTextBox.SelectedText = string.Format("{0:N2}", selected.ql);
                    scrapTextBox.SelectedText = string.Format("{0:N2}", selected.qs);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            supplierCodeTextBox.Text = "";
            supplierTextBox.Text = "";
            THsupplierTextBox.Text = "";
            areaTextBox.Text = "";
            hightextBox.Text = "";
            mediumTextBox.Text = "";
            lowTextBox.Text = "";
            scrapTextBox.Text = "";
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            ShowAllSupplier();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string s_nameth = "";
                string s_area = "";
                float high = 0;
                float medium = 0;
                float low = 0;
                float scrap = 0;
                if (THsupplierTextBox.Text != "")
                    s_nameth = supplierTextBox.Text;
                if (areaTextBox.Text != "")
                    s_area = areaTextBox.Text;
                if (hightextBox.Text != "")
                    high = (float)Convert.ToDouble(hightextBox.Text);
                if (mediumTextBox.Text != "")
                    medium = (float)Convert.ToDouble(mediumTextBox.Text);
                if (lowTextBox.Text != "")
                    low = (float)Convert.ToDouble(lowTextBox.Text);
                if (scrapTextBox.Text != "")
                    scrap = (float)Convert.ToDouble(scrapTextBox.Text);

                if (ConfirmProcess())
                {
                    var supp = new supplier
                    {
                        code = supplierCodeTextBox.Text,
                        name = supplierTextBox.Text,
                        nameth = s_nameth,
                        area = s_area,
                        qh = (float)high,
                        qm = (float)medium,
                        ql = (float)low,
                        qs = (float)scrap
                    };
                    var existedSupp = BussinessLayerService.SupplierBL().ExistedSupp(supp.code);
                    if (existedSupp)
                        EditSupp(supp);
                    else
                        InsertNewSupp(supp);

                    ShowAllSupplier();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void InsertNewSupp(supplier supp)
        {
            try
            {
                BussinessLayerService.SupplierBL().AddNewSupp(supp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditSupp(supplier supp)
        {
            try
            {
                BussinessLayerService.SupplierBL().UpdateSupp(supp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public bool ConfirmProcess()
        {
            bool cf = true;
            if (string.IsNullOrEmpty(supplierCodeTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ supplier code", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(supplierTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ชื่อ supplier", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }         
            return cf;
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (supplierDataGrid.SelectedIndex < 0)
                return;

            if (string.IsNullOrEmpty(supplierCodeTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ supplier code", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;

            var supp = new supplier
            {
                code = supplierCodeTextBox.SelectedText.ToString(),
                name = supplierTextBox.SelectedText.ToString(),
                nameth = THsupplierTextBox.SelectedText.ToString()            
            };
            BussinessLayerService.SupplierBL().Deletesupp(supp);

            ShowAllSupplier();
        }
    }
}
