﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Windows.Input;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for PickingPage.xaml
    /// </summary>


    public partial class PickingPage : Page
    {
        string currentUser = "";
        public PickingPage(string user)
        {
            InitializeComponent();
            currentUser = user;
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                //type
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();

                TypeComboBox.Text = "BU";
                ShowPickingInfo(TypeComboBox.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowPickingInfo(string t)
        {
            pickingDataGrid.ItemsSource = null;
            var existed = BussinessLayerService.pickingBL().GetPickingByType(t);
            if (existed.Count() > 0) pickingDataGrid.ItemsSource = existed;
        }

        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            ShowPickingInfo(TypeComboBox.Text);
        }

        private void pickingDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (pickingDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();
                var selected = (picking)pickingDataGrid.SelectedItem;
                if (selected != null)
                {
                    gradeTextBox.Text = selected.classify;
                    marktextBox.Text = selected.mark;
                    pricetextBox.Text = string.Format("{0:N2}", selected.price);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            gradeTextBox.Text = "";
            marktextBox.Text = "";
            pricetextBox.Text = "";
        }
        private bool verifyAllInfo()
        {
            //check null
            bool cf = true;
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                TypeComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(gradeTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ชื่อเกรด", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                gradeTextBox.Focus();
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(pricetextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ราคา", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                pricetextBox.Focus();
                cf = false;
                return cf;
            }

            return cf;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                if (BussinessLayerService.pickingBL().ExistedPicking(TypeComboBox.Text,gradeTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถเพิ่มเกรดนี้ได้ เนื่องจากในระบบมีเกรดนี้อยู่แล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                var NewPick = new picking
                {
                    type = TypeComboBox.Text,
                    classify = gradeTextBox.Text,
                    mark = marktextBox.Text,
                    price = Convert.ToDecimal(pricetextBox.Text),
                    dtrecord = DateTime.Now,
                    user = currentUser
                };
                BussinessLayerService.pickingBL().AddNewPicking(NewPick);
                MessageBox.Show("เพิ่ม Picking Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowPickingInfo(TypeComboBox.Text);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                if (!BussinessLayerService.pickingBL().ExistedPicking(TypeComboBox.Text, gradeTextBox.Text))
                {
                    MessageBox.Show("ไม่พบเกรดนี้อยู่ในระบบ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                var exPick = new picking
                {
                    type = TypeComboBox.Text,
                    classify = gradeTextBox.Text,
                    mark = marktextBox.Text,
                    price = Convert.ToDecimal(pricetextBox.Text),
                    dtrecord = DateTime.Now,
                    user = currentUser
                };
                BussinessLayerService.pickingBL().UpdatePicking(exPick);
                MessageBox.Show("แก้ไข Picking Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowPickingInfo(TypeComboBox.Text);
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {

            if (verifyAllInfo())
            {
                if (!BussinessLayerService.pickingBL().ExistedPicking(TypeComboBox.Text, gradeTextBox.Text))
                {
                    MessageBox.Show("ไม่พบเกรดนี้อยู่ในระบบ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                if (MessageBox.Show("ต้องการลบ Picking Grade นี้ใช่หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {

                    var exPick = new picking
                    {
                        type = TypeComboBox.Text,
                        classify = gradeTextBox.Text,
                        mark = marktextBox.Text,
                        price = Convert.ToDecimal(pricetextBox.Text),
                        dtrecord = DateTime.Now,
                        user = currentUser
                    };
                    BussinessLayerService.pickingBL().DeletePicking(exPick);
                    MessageBox.Show("ลบ Picking Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ShowPickingInfo(TypeComboBox.Text);
                }
            }
        }
    }
}
