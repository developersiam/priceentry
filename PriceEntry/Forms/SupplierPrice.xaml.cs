﻿using System;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Collections.Generic;
using System.Linq;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for SupplierPrice.xaml
    /// </summary>
    public partial class SupplierPrice : Page
    {
        int current_crop;
        string userName = "";
        public SupplierPrice(string uName)
        {
            InitializeComponent();
            current_crop = DateTime.Now.Year;
            userName = uName;
            BlindingData();
        }

        private void BlindingData()
        {
           try
            {
                //supplier
                supplierComboBox.ItemsSource = null;
                supplierComboBox.ItemsSource = BussinessLayerService.SupplierBL().GetAllSupplierCode();
                //type
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            string s_name = "";
            if (!string.IsNullOrEmpty(supplierTextBox.Text))
                s_name = supplierTextBox.Text;

            if (ConfirmProcess())
            {
                var charge = new suppliercharge
                {
                    code = supplierComboBox.SelectedValue.ToString(),
                    name = s_name,
                    type = TypeComboBox.SelectedValue.ToString(),
                    green = greenComboBox.SelectedValue.ToString(),
                    farmer_bonus = Convert.ToDouble(farmerTextBox.Text),
                    tcharge = Convert.ToDouble(transporttextBox.Text),
                    fee = Convert.ToDouble(feeTextBox.Text),
                    service_fee = Convert.ToDouble(serviceTextBox.Text),
                    tw_fee = Convert.ToDouble(twTextBox.Text),
                    dtrecord = DateTime.Now,
                    user = userName
                };
                var existedCharge = BussinessLayerService.SupplierChargeBL().ExistedCharge(charge.code, charge.type, charge.green);
                if (existedCharge)
                    EditCharge(charge);
                else
                    InsertNewCharge(charge);

                ShowAllGradeInfo();
            }
        }

        private void InsertNewCharge(suppliercharge charge)
        {
            try
            {
                BussinessLayerService.SupplierChargeBL().AddNewCharge(charge);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void EditCharge(suppliercharge charge)
        {
            try
            {
                BussinessLayerService.SupplierChargeBL().UpdateCharge(charge);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void showButton_Click(object sender, RoutedEventArgs e)
        {
            ShowAllGradeInfo();
        }

        private void ShowAllGradeInfo()
        {
            try
            {
                //Show by supplier
                if (string.IsNullOrEmpty(supplierComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก supplier", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(TypeComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก Type", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var existed = BussinessLayerService.SupplierChargeBL().GetSupplierAllGrade(supplierComboBox.SelectedValue.ToString(), TypeComboBox.SelectedValue.ToString());
                if (existed.Count() > 0 )
                    pricingDataGrid.ItemsSource = existed;
                else
                    pricingDataGrid.ItemsSource = null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void pricingDataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
                if (pricingDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();

                var selected = (suppliercharge)pricingDataGrid.SelectedItem;
                if (selected != null)
                {
                    supplierComboBox.SelectedValue = selected.code;
                    supplierTextBox.SelectedText = selected.name;
                    TypeComboBox.SelectedValue = selected.type;
                    greenComboBox.SelectedValue = selected.green;
                    farmerTextBox.SelectedText = string.Format("{0:N2}", selected.farmer_bonus);
                    transporttextBox.SelectedText = string.Format("{0:N2}", selected.tcharge);
                    feeTextBox.SelectedText = string.Format("{0:N2}", selected.fee);
                    serviceTextBox.SelectedText = string.Format("{0:N2}", selected.service_fee);
                    twTextBox.SelectedText = string.Format("{0:N2}", selected.tw_fee);
                };

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            supplierTextBox.Text = "";
            greenComboBox.Text = "";
            transporttextBox.Text = "";
            feeTextBox.Text = "";
            serviceTextBox.Text = "";
            twTextBox.Text = "";
            farmerTextBox.Text = "";
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (pricingDataGrid.SelectedIndex < 0)
                return;

            if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                return;

            //check every textbox bf delete
            if (ConfirmProcess())
            {
                var charge = new suppliercharge
                {
                    code = supplierComboBox.SelectedValue.ToString(),
                    type = TypeComboBox.SelectedValue.ToString(),
                    green = greenComboBox.SelectedValue.ToString(),
                    farmer_bonus = Convert.ToDouble(farmerTextBox.Text),
                    tcharge = Convert.ToDouble(transporttextBox.Text),
                    fee = Convert.ToDouble(feeTextBox.Text),
                    service_fee = Convert.ToDouble(serviceTextBox.Text),
                    tw_fee = Convert.ToDouble(twTextBox.Text)
                };
                BussinessLayerService.SupplierChargeBL().DeleteCharge(charge);
            }
            ShowAllGradeInfo();
        }

        public bool ConfirmProcess()
        {
            bool cf = true;
            if (string.IsNullOrEmpty(supplierComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก supplier", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(greenComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Green Grade", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(transporttextBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Transport Charge", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(farmerTextBox.Text))
            {
                MessageBox.Show("กรุณาเลือก farmer Bonus", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(feeTextBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Procurement Charge", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(serviceTextBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Service Charge", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(twTextBox.Text))
            {
                MessageBox.Show("กรุณาเลือก TW Charge", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            return cf;
        }

        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            //green
            greenComboBox.ItemsSource = null;
            if (!string.IsNullOrEmpty(TypeComboBox.Text))
            {
                var greenGrade = BussinessLayerService.StoreProcedureBL().GetGreenCode(current_crop, TypeComboBox.SelectedValue.ToString());
                if (greenGrade.Count() > 0)
                    greenComboBox.ItemsSource = greenGrade.OrderBy(c => c.level).Select(w => w.green).ToList();

                //Show all grade under this type
                ShowAllGradeInfo();

            }
        }

        private void supplierComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ClearCurrent();
            //type
            TypeComboBox.ItemsSource = null;
            TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();
            //Clear Grid
            pricingDataGrid.ItemsSource = null;

            if (!string.IsNullOrEmpty(supplierComboBox.Text))
            {
                supplierTextBox.Text = "";
                var spName = BussinessLayerService.SupplierBL().GetSupplierName(supplierComboBox.SelectedValue.ToString());
                if (spName != "")
                    supplierTextBox.Text = spName;
            }
        }

        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Check all input not null
                if (string.IsNullOrEmpty(supplierComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก supplier", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (string.IsNullOrEmpty(TypeComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก Type", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                SupplierCopy c = new SupplierCopy(supplierComboBox.Text,TypeComboBox.Text, userName);
                c.ShowDialog();
             }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }
    }
}
