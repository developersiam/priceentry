﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Windows.Input;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for PriceSetupPage.xaml
    /// </summary>
    public partial class PriceSetupPage : Page
    {
        string currentUser = "";
        public PriceSetupPage(string user)
        {
            InitializeComponent();
            BlindingInfo();
            currentUser = user;
        }

        private void BlindingInfo()
        {
            //Type
            TypeComboBox.ItemsSource = null;
            TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();

        }

        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            ClearCurrent();
            ShowPriceByType();
        }

        private void ShowPriceByType()
        {
            try
            {
                PriceDataGrid.ItemsSource = null;
                totalGreenTextBlock.Text = "0";
                if (string.IsNullOrEmpty(TypeComboBox.Text))
                {
                    MessageBox.Show("กรุณาเลือก Type", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (string.IsNullOrEmpty(cropTextBox.Text))
                {
                    MessageBox.Show("กรุณาใส่ Crop", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var greenGrade = BussinessLayerService.StoreProcedureBL().GetGreenCode(Convert.ToInt16(cropTextBox.Text), TypeComboBox.SelectedValue.ToString());
                if (greenGrade.Count() > 0)
                {
                    PriceDataGrid.ItemsSource = greenGrade.OrderBy(c => c.green).ToList();
                    totalGreenTextBlock.Text = greenGrade.Count().ToString();
                }             
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void PriceDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (PriceDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();

                var selected = (sp_Sel_Green_All_By_Crop_Result)PriceDataGrid.SelectedItem;
                if (selected != null)
                {
                    greenTextBox.Text = selected.green;
                    gropTextBox.Text = selected.group.ToString();
                    P1TextBox.Text = string.Format("{0:N2}", selected.price1);
                    P2TextBox.Text = string.Format("{0:N2}", selected.price2);
                    P3TextBox.Text = string.Format("{0:N2}", selected.price3);
                    P4TextBox.Text = string.Format("{0:N2}", selected.price4);
                    P5TextBox.Text = string.Format("{0:N2}", selected.price5);
                    P6TextBox.Text = string.Format("{0:N2}", selected.price6);
                    P7TextBox.Text = string.Format("{0:N2}", selected.price7);
                    P8TextBox.Text = string.Format("{0:N2}", selected.price8);
                    P9TextBox.Text = string.Format("{0:N2}", selected.price9);
                    P10TextBox.Text = string.Format("{0:N2}", selected.price10);
                    P11TextBox.Text = string.Format("{0:N2}", selected.price11);
                    P12TextBox.Text = string.Format("{0:N2}", selected.price12);
                    P13TextBox.Text = string.Format("{0:N2}", selected.price13);
                    P14TextBox.Text = string.Format("{0:N2}", selected.price14);
                    P15TextBox.Text = string.Format("{0:N2}", selected.price15);
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            greenTextBox.Text = "";
            gropTextBox.Text = "";
            P1TextBox.Text = "";
            P2TextBox.Text = "";
            P3TextBox.Text = "";
            P4TextBox.Text = "";
            P5TextBox.Text = "";
            P6TextBox.Text = "";
            P7TextBox.Text = "";
            P8TextBox.Text = "";
            P9TextBox.Text = "";
            P10TextBox.Text = "";
            P11TextBox.Text = "";
            P12TextBox.Text = "";
            P13TextBox.Text = "";
            P14TextBox.Text = "";
            P15TextBox.Text = "";
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {

            if (verifyAllInfo())
            {
                var GreenModel = CreateModel();

                if (BussinessLayerService.greenBL().ExistedGreen(Convert.ToInt16(cropTextBox.Text), TypeComboBox.Text, greenTextBox.Text))
                {
                    BussinessLayerService.greenBL().UpdateGreen(GreenModel);
                }
                else
                {
                    BussinessLayerService.greenBL().AddNewGreen(GreenModel);
                }
                                 
                MessageBox.Show("Update Green Price เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowPriceByType();
            }
        }
        private bool verifyAllInfo()
        {
            //check null
            bool cf = true;
            if (string.IsNullOrEmpty(cropTextBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Crop", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                cropTextBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                TypeComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(greenTextBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Green grade", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                greenTextBox.Focus();
                cf = false;
                return cf;
            }
            return cf;
        }
        private green CreateModel()
        {
            var NewGreen = new green
            {
                crop = Convert.ToInt16(cropTextBox.Text),
                type = TypeComboBox.Text,
                green1 = greenTextBox.Text,
                group = gropTextBox.Text != "" ? Convert.ToInt16(gropTextBox.Text) : (int?)null,
                p1 = P1TextBox.Text != "" ? Convert.ToDecimal(P1TextBox.Text) : (decimal?)null,
                p2 = P2TextBox.Text != "" ? Convert.ToDecimal(P2TextBox.Text) : (decimal?)null,
                p3 = P3TextBox.Text != "" ? Convert.ToDecimal(P3TextBox.Text) : (decimal?)null,
                p4 = P4TextBox.Text != "" ? Convert.ToDecimal(P4TextBox.Text) : (decimal?)null,
                p5 = P5TextBox.Text != "" ? Convert.ToDecimal(P5TextBox.Text) : (decimal?)null,
                p6 = P6TextBox.Text != "" ? Convert.ToDecimal(P6TextBox.Text) : (decimal?)null,
                p7 = P7TextBox.Text != "" ? Convert.ToDecimal(P7TextBox.Text) : (decimal?)null,
                p8 = P8TextBox.Text != "" ? Convert.ToDecimal(P8TextBox.Text) : (decimal?)null,
                p9 = P9TextBox.Text != "" ? Convert.ToDecimal(P9TextBox.Text) : (decimal?)null,
                p10 = P10TextBox.Text != "" ? Convert.ToDecimal(P10TextBox.Text) : (decimal?)null,
                p11 = P11TextBox.Text != "" ? Convert.ToDecimal(P11TextBox.Text) : (decimal?)null,
                p12 = P12TextBox.Text != "" ? Convert.ToDecimal(P12TextBox.Text) : (decimal?)null,
                p13 = P13TextBox.Text != "" ? Convert.ToDecimal(P13TextBox.Text) : (decimal?)null,
                p14 = P14TextBox.Text != "" ? Convert.ToDecimal(P14TextBox.Text) : (decimal?)null,
                p15 = P15TextBox.Text != "" ? Convert.ToDecimal(P15TextBox.Text) : (decimal?)null,
                dtrecord = DateTime.Now,
                rcuser = currentUser
            };

            return NewGreen;

        }
    }
}
