﻿using System;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Collections.Generic;
using System.Linq;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for SupplierCopy.xaml
    /// </summary>
    public partial class SupplierCopy : Window
    {
        string fromSupp = "";
        string fromType = "";
        string UserName = "";
        public SupplierCopy(string SuppId, string fType, string Uname)
        {
            InitializeComponent();
            fromSupp = SuppId;
            fromType = fType;
            UserName = Uname;
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                //from supplier
                if (!string.IsNullOrEmpty(fromSupp))
                {
                    fromIDTextBox.Text = fromSupp;
                    fromTypeTextBox.Text = fromType;
                    var spName = BussinessLayerService.SupplierBL().GetSupplierName(fromSupp);
                    if (spName != "")
                        fromNameTextBox.Text = spName;
                }
                //to supplier
                supplierComboBox.ItemsSource = null;
                supplierComboBox.ItemsSource = BussinessLayerService.SupplierBL().GetAllSupplierCode();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void supplierComboBox_DropDownClosed(object sender, EventArgs e)
        {
            //from supplier
            if (string.IsNullOrEmpty(supplierComboBox.Text)) return;
            if (!string.IsNullOrEmpty(supplierComboBox.SelectedValue.ToString()))
            {
                var spName = BussinessLayerService.SupplierBL().GetSupplierName(supplierComboBox.SelectedValue.ToString());
                if (spName != "")
                    toNameTextBox.Text = spName;
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckAllInfo())
                {
                    //Add
                    BussinessLayerService.SupplierChargeBL().ReproductionSupplier(fromIDTextBox.Text, supplierComboBox.SelectedValue.ToString(), toNameTextBox.Text, fromTypeTextBox.Text, UserName);
                    MessageBox.Show("ทำการ Reproduction ข้อมูลสำเร็จแล้ว", "Success!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CheckAllInfo()
        {
            //Check empty field
            bool cf = true;
            if (string.IsNullOrEmpty(supplierComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Supplier", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(fromIDTextBox.Text))
            {
                MessageBox.Show("Supplier ID เป็นค่าว่างไม่ได้", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }

            if (string.IsNullOrEmpty(fromTypeTextBox.Text))
            {
                MessageBox.Show("Type เป็นค่าว่างไม่ได้", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            //Check existed to supplier
            var existedCharge = BussinessLayerService.SupplierChargeBL()
                .ExistedSupplierCharge(supplierComboBox.Text, fromTypeTextBox.Text);
            if (existedCharge)
            {
                MessageBox.Show("Supplier :" + supplierComboBox.Text + " มีการ set ค่าอยู่แล้ว กรุณาเช็คก่อนทำ Reproduction", "Warning!",
                       MessageBoxButton.OK, MessageBoxImage.Warning);
                cf = false;
                return cf;
            }
            else
                cf = true;

            return cf;
        }
    }
}
