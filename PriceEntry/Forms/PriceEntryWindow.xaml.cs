﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Windows.Input;
using System.Windows.Media;
using PriceEntry.Reports;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for PriceEntryWindow.xaml
    /// </summary>
    public partial class PriceEntryWindow : Window
    {
        string CurrentUser = "";
        decimal total_weight = 0;
        double total_price = 0;
        double total_price_supplier = 0;
        int? DocNo = 0;

        public PriceEntryWindow(string user, sp_GreenLeaf_SEL_ReceivingDocInfo_Result rcInfo)
        {
            InitializeComponent();
            Blinding(rcInfo);
            CurrentUser = user;
        }

        private void Blinding(sp_GreenLeaf_SEL_ReceivingDocInfo_Result rcInfo)
        {
            //Show all receiving info
            if (rcInfo != null)
            {
                //Check Supplier Price Setup
                ShowSupplierPriceSetup(rcInfo);

                //Check status
                ShowStatusDocument(rcInfo.leaflocked);

                DocNo = rcInfo.docno;
                //Listing all info and Calculate Total
                ListingDocumentInfo(rcInfo.rcno, DocNo);

                receivingNoTextBox.Text = rcInfo.rcno;
                rcFromTextBox.Text = rcInfo.rcfrom;
                placeTextBox.Text = rcInfo.place;
                dateTextBox.Text = rcInfo.date.ToString();
                buyerTextBox.Text = rcInfo.buyer;
                classifierTextBox.Text = rcInfo.classifier;
                truckTextBox.Text = rcInfo.truckno;
                balesTotalTextBox.Text = rcInfo.bt.ToString();
                cropTextBox.Text = rcInfo.crop.ToString();
                typeTextBox.Text = rcInfo.type;
            };

        }

        private void ShowSupplierPriceSetup(sp_GreenLeaf_SEL_ReceivingDocInfo_Result rc)
        {
            try
            {
                string priceSetup;
                if ((bool)rc.leaflocked)
                {
                    priceComboBox.Text = BussinessLayerService.matBL().GetMatListByRcNo(rc.rcno).FirstOrDefault().priceno.ToString();
                        
                }
                else
                {
                    priceSetup = BussinessLayerService.suppliergreenpriceBL().GetPriceNo((int)rc.crop, rc.supplier, rc.type, rc.company).ToString();
                    if (priceSetup != "0") priceComboBox.Text = priceSetup;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ListingDocumentInfo(string rcno, int? docno)
        {
            try
            {
                total_weight = 0;
                total_price = 0;
                total_price_supplier = 0;

                List<mat> rec_detail = new List<mat>();
                rec_detail.AddRange(BussinessLayerService.matBL().GetMatListByRcNo(rcno).Where(rc => rc.docno == docno));
                if (rec_detail != null)
                {
                    foreach(var i in rec_detail)
                    {
                        total_weight += (i.company == "10" ? (decimal)i.weight : (decimal)i.weightbuy);

                        //price total
                        total_price += (double)i.price;
                        total_price_supplier += (double)i.price_wo_handle_charge;
                    }

                    priceAvgTextBox.Text = (Convert.ToDecimal(total_price) / total_weight).ToString("N2");
                    priceTotalTextBox.Text = total_price.ToString("N2");
                    weightTotalTextBox.Text = total_weight.ToString("N2");
                    //Blinding Data to grid
                    detailsDataGrid.ItemsSource = rec_detail;
                }


            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ShowReceivingDetails(string rcno)
        {
            var details = BussinessLayerService.matBL().GetMatListByRcNo(rcno);
            if (details != null) detailsDataGrid.ItemsSource = details;

        }

        private void ShowStatusDocument(bool? leaflocked)
        {
            try
            {
                if ((bool)leaflocked)
                {
                    statusTextBox.Text = "Locked";
                    statusTextBox.Background = Brushes.Red;
                    executeButton.IsEnabled = false;
                    finishButton.IsEnabled = false;
                }
                else
                {
                    statusTextBox.Text = "Unlock";
                    statusTextBox.Background = Brushes.LightGreen;
                    executeButton.IsEnabled = true;
                    finishButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void executeButton_Click(object sender, RoutedEventArgs e)
        {
            if (priceComboBox.Text == "")
            {
                MessageBox.Show("กรุณาระบุ Price No.", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                priceComboBox.Focus();
                return;
            }

            //Calculate Price per bales
            CalculatePrice(priceComboBox.Text);
        }

        private void CalculatePrice(string priceNo)
        {
            try
            {
                double tranC = 0;
                double feeC = 0;
                double serviceC = 0;
                double bonusC = 0;
                double twC = 0;
                double HC = 0;
                decimal unitPrice = 0;
                string tmpColumnPrice = "";
                decimal tmpWeight = 0;
                tmpColumnPrice = "p" + priceNo;

                List<green> GetAllGreenPrice = BussinessLayerService.greenBL().GetAllGreenByCropType(Convert.ToDecimal(cropTextBox.Text), typeTextBox.Text);
                List<suppliercharge> GetAllSupplierByType = BussinessLayerService.SupplierChargeBL().GetSupplierByType(typeTextBox.Text);

                //Get all barcode for calculate
                List<mat> rec_detail = new List<mat>();
                rec_detail.AddRange(BussinessLayerService.matBL().GetMatListByRcNo(receivingNoTextBox.Text).Where(rc => rc.docno == DocNo));


                if (rec_detail != null)
                {
                    foreach (var i in rec_detail)
                    {
                        ///////////////////////////////// PRICE PER GRADE /////////////////////////////////////
                        //get all price from grade
                        var selected_green = GetAllGreenPrice.Where(r => r.green1 == i.green);
                        if (!selected_green.Any())
                        {
                            MessageBox.Show("ไม่พบเกรด " + i.green.ToString() + " นี้ ในตารางราคา", "Warning!",
                            MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }
                        //get unit price
                        unitPrice = Convert.ToDecimal(GetSelectedPrice(selected_green,tmpColumnPrice).First());

                        /////////////////////////////// FEE PER SUPPLIER /////////////////////////////////////
                        //get all fee for this supplier
                        tranC = 0;
                        feeC = 0;
                        serviceC = 0;
                        bonusC = 0;
                        twC = 0;
                        HC = 0;
                        tmpWeight = 0;
                        var selected_fee = GetAllSupplierByType.Where(s => s.code == i.supplier && s.green == i.green);
                        if (selected_fee.Any())
                        {
                            tranC = (double)selected_fee.First().tcharge;
                            feeC = (double)selected_fee.First().fee;
                            serviceC = (double)selected_fee.First().service_fee;
                            bonusC = (double)selected_fee.First().farmer_bonus;
                            twC = (double)selected_fee.First().tw_fee;

                            HC = feeC + serviceC + bonusC + twC;
                        }

                        tmpWeight = (i.company == "10" ? (decimal)i.weight : (decimal)i.weightbuy);

                        //////////////////////////////// UPDATE ///////////////////////////////////
                        UpdateBalePrice(i.bc, tmpWeight, unitPrice, HC, tranC);
                    }

                    ListingDocumentInfo(receivingNoTextBox.Text, DocNo);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UpdateBalePrice(string selected_bc, decimal tmpWeight, decimal unitPrice, double hC, double tranC)
        {
            try
            {
                var selected_bale = new mat
                {
                    bc = selected_bc,
                    priceno = Convert.ToInt16(priceComboBox.Text),
                    priceunit = Math.Round((Convert.ToDouble(unitPrice) + hC + tranC), 2, MidpointRounding.AwayFromZero),
                    price = Math.Round((Convert.ToDouble(tmpWeight) * (Convert.ToDouble(unitPrice) + hC + tranC)),2 , MidpointRounding.AwayFromZero),
                    tcharge = Convert.ToDecimal(tranC),
                    handle_charge = hC,
                    priceunit_wo_handle_charge = Convert.ToDouble(unitPrice),
                    price_wo_handle_charge = Math.Round((Convert.ToDouble(tmpWeight) * (Convert.ToDouble(unitPrice) + tranC)),2 , MidpointRounding.AwayFromZero)
                };
                BussinessLayerService.matBL().UpdatePricePerBales(selected_bale);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private IEnumerable<object> GetSelectedPrice(IEnumerable<green> selected_green, string tmpColumnPrice)
        {
            var value = selected_green.Select(u => u.GetType().GetProperty(tmpColumnPrice).GetValue(u));
            return value;
        }

        private void finishButton_Click(object sender, RoutedEventArgs e)
        {
            //Check Price = 0
            var none_price = BussinessLayerService.matBL().GetMatListByRcNo(receivingNoTextBox.Text).Where(rc => rc.docno == DocNo && rc.price == 0);
            if (none_price.Count() > 0)
            {
                if (MessageBox.Show("ระบบพบว่ามียาบางรายการ ราคา=0 ถ้าต้องการให้ระบบทำงานต่อกด Yes, แต่ถ้าไม่ ให้กด No", "Warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
            }

            //Update leaflocked=1 in Mat table
            BussinessLayerService.matBL().UpdateLeafLocked(DocNo, receivingNoTextBox.Text);

            ShowStatusDocument(true);
            ListingDocumentInfo(receivingNoTextBox.Text, DocNo); 
        }

        private void SummarySuppMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(receivingNoTextBox.Text))
            {
                Reports.RPTSummarySupplier window = new Reports.RPTSummarySupplier(receivingNoTextBox.Text, (int)DocNo);
                window.ShowDialog();
            }
        }
        private void SummaryStecMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(receivingNoTextBox.Text))
            {
                Reports.RPTSummaryStec window = new Reports.RPTSummaryStec(receivingNoTextBox.Text, (int)DocNo);
                window.ShowDialog();
            }
        }

        private void DetailsSuppMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(receivingNoTextBox.Text))
            {
                Reports.RPTDetailsSupplier window = new Reports.RPTDetailsSupplier(receivingNoTextBox.Text, (int)DocNo);
                window.ShowDialog();
            }
        }

        private void DetailsStecMenu_Click(object sender, RoutedEventArgs e)
        {
            if (!String.IsNullOrEmpty(receivingNoTextBox.Text))
            {
                Reports.RPTDetailsStec window = new Reports.RPTDetailsStec(receivingNoTextBox.Text, (int)DocNo);
                window.ShowDialog();
            }
        }
    }
}
