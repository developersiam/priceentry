﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using DomainModel;
using PriceEntryBL;
using System.Windows.Input;

namespace PriceEntry.Forms
{
    /// <summary>
    /// Interaction logic for GreenPage.xaml
    /// </summary>
    public partial class GreenPage : Page
    {
        string currentUser = "";
        public GreenPage(string user)
        {
            InitializeComponent();
            currentUser = user;
            Blinding();
        }

        private void Blinding()
        {
            try
            {
                //type
                TypeComboBox.ItemsSource = null;
                TypeComboBox.ItemsSource = BussinessLayerService.TypeBL().GetAllTypes();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning !!", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TypeComboBox.Text) && string.IsNullOrEmpty(cropTextBox.Text))
            {
                MessageBox.Show("กรุณาใส่ Crop และ Type ให้ครบถ้วน", "Warning!",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            ShowGreenGradeInfo(TypeComboBox.Text, Convert.ToDecimal(cropTextBox.Text));
        }

        private void ShowGreenGradeInfo(string t,decimal crop)
        {
            greenDataGrid.ItemsSource = null;
            totalGreenTextBlock.Text = "0";
            var existed = BussinessLayerService.greenBL().GetAllGreenByCropType(crop, t);
            if (existed.Count() > 0)
            {
                greenDataGrid.ItemsSource = existed;
                totalGreenTextBlock.Text = existed.Count().ToString();
            }               
        }

        private void greenDataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (greenDataGrid.SelectedIndex < 0)
                    return;

                ClearCurrent();
                var selected = (green)greenDataGrid.SelectedItem;
                if (selected != null)
                {
                    greenTextBox.SelectedText = selected.green1;
                    levelTextBox.SelectedText = selected.level.ToString();
                    groupTextBox.SelectedText = selected.group.ToString();
                };

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearCurrent()
        {
            greenTextBox.Text = "";
            levelTextBox.Text = "";
            groupTextBox.Text = "";
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                if (BussinessLayerService.greenBL().ExistedGreen(Convert.ToInt16(cropTextBox.Text),TypeComboBox.Text,greenTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถเพิ่มเกรดนี้ได้ เนื่องจากในระบบมีเกรดนี้อยู่แล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                var NewGreen = new green
                {
                  crop = Convert.ToInt16(cropTextBox.Text),  
                  type = TypeComboBox.Text,
                  green1 = greenTextBox.Text,
                  level = levelTextBox.Text != "" ? Convert.ToInt16(levelTextBox.Text) : (int?)null,
                  group = groupTextBox.Text != "" ? Convert.ToInt16(groupTextBox.Text) : (int?)null,
                  dtrecord = DateTime.Now,
                  rcuser = currentUser
                };
                BussinessLayerService.greenBL().AddNewGreen(NewGreen);
                MessageBox.Show("เพิ่ม Green Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowGreenGradeInfo(TypeComboBox.Text,Convert.ToInt16(cropTextBox.Text));
            }

        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                if (!BussinessLayerService.greenBL().ExistedGreen(Convert.ToInt16(cropTextBox.Text), TypeComboBox.Text, greenTextBox.Text))
                {
                    MessageBox.Show("ไม่พบเกรดนี้อยู่ในระบบ", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }
                //Check green used in mat table already
                if (BussinessLayerService.matBL().CheckGreenUsed(Convert.ToInt16(cropTextBox.Text), TypeComboBox.Text, greenTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถแก้ไขเกรด Green นี้ได้ เนื่องจากในระบบมีการใช้เกรดและราคาของเกรดนี้ไปแล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                var exGreen = new green
                {
                    crop = Convert.ToInt16(cropTextBox.Text),
                    type = TypeComboBox.Text,
                    green1 = greenTextBox.Text,
                    level = levelTextBox.Text != "" ? Convert.ToInt16(levelTextBox.Text) : (int?)null,
                    group = groupTextBox.Text != "" ? Convert.ToInt16(groupTextBox.Text) : (int?)null,
                    dtrecord = DateTime.Now,
                    rcuser = currentUser
                };
                BussinessLayerService.greenBL().UpdateGreen(exGreen);
                MessageBox.Show("แก้ไข Green Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowGreenGradeInfo(TypeComboBox.Text, Convert.ToInt16(cropTextBox.Text));
            }
        }

        private void deleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (verifyAllInfo())
            {
                //Check green used in mat table already
                if (BussinessLayerService.matBL().CheckGreenUsed(Convert.ToInt16(cropTextBox.Text), TypeComboBox.Text, greenTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถลบเกรด Green นี้ได้ เนื่องจากระบบ Receiving มีการใช้เกรดนี้ไปแล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                if (MessageBox.Show("ต้องการลบ Green Grade นี้ใช่หรือไม่", "Confirmation!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    var i = new green
                    {
                        crop = Convert.ToInt16(cropTextBox.Text),
                        green1 = greenTextBox.Text,
                        type = TypeComboBox.Text
                    };
                    BussinessLayerService.greenBL().DeleteGreen(i);
                    MessageBox.Show("ลบข้อมูลเกรด Green Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                    ShowGreenGradeInfo(TypeComboBox.Text, Convert.ToInt16(cropTextBox.Text));
                }
            }
        }

        private bool verifyAllInfo()
        {
            //check null
            bool cf = true;
            if (string.IsNullOrEmpty(cropTextBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Crop", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                cropTextBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(TypeComboBox.Text))
            {
                MessageBox.Show("กรุณาเลือก Type", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                TypeComboBox.Focus();
                cf = false;
                return cf;
            }
            if (string.IsNullOrEmpty(greenTextBox.Text))
            {
                MessageBox.Show("กรุณาระบุ Green grade", "Warning!",
                MessageBoxButton.OK, MessageBoxImage.Warning);
                greenTextBox.Focus();
                cf = false;
                return cf;
            }
            return cf;
        }

        private void greenTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            if (verifyAllInfo())
            {
                if (BussinessLayerService.greenBL().ExistedGreen(Convert.ToInt16(cropTextBox.Text), TypeComboBox.Text, greenTextBox.Text))
                {
                    MessageBox.Show("ไม่สามารถเพิ่มเกรดนี้ได้ เนื่องจากในระบบมีเกรดนี้อยู่แล้ว", "Warning!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return;
                }

                var NewGreen = new green
                {
                    crop = Convert.ToInt16(cropTextBox.Text),
                    type = TypeComboBox.Text,
                    green1 = greenTextBox.Text,
                    level = levelTextBox.Text != "" ? Convert.ToInt16(levelTextBox.Text) : (int?)null,
                    group = groupTextBox.Text != "" ? Convert.ToInt16(groupTextBox.Text) : (int?)null,
                    dtrecord = DateTime.Now,
                    rcuser = currentUser
                };
                BussinessLayerService.greenBL().AddNewGreen(NewGreen);
                MessageBox.Show("เพิ่ม Green Grade เรียบร้อยแล้ว", "Complete!", MessageBoxButton.OK, MessageBoxImage.Information);
                ShowGreenGradeInfo(TypeComboBox.Text, Convert.ToInt16(cropTextBox.Text));
                addButton.Focus();
            }
        }
    }
}
